local _goto
_goto = function(self, label)
	for index, line in ipairs(self.lines) do
		line = line:trim():gsub("%/%w+%/", "")
		if line:take(1) == '#' then
			if line:skip(1):trim() == label then
				self.inside_block = false
				self.position = index - 1
				return 
			end
		end
	end
end
local _return
_return = function(self, func)
	self.script = nil
	self.type = nil
	return func()
end
local nothing
nothing = function(self)
	if self.inside_block then
		self.position = self.inside_block_outer - 1
		self.inside_block = false
	end
	return self:next()
end
local shortcut
shortcut = function(self, name)
	return 'dialog.scene.context.'
end
local shortcut2
shortcut2 = function(self, name)
	return 'dialog.scene.bidons.'
end
local sprite
sprite = function(self, sprite)
	self.sprite = 'assets/sprites/' .. sprite .. '.png'
end
local sprite_off
sprite_off = function(self)
	self.sprite = nil
end
local scene
scene = function(self, scene, params)
	self:end_dialog()
	return self.scene.bidons.konstabel:entrust(scene, params)
end
local virtual_bnf
virtual_bnf = function(self, self)
	return nil
end
local update
update = function(self, dialog)
	for _, value in pairs(self.scene.raw_list) do
		value:run('updateBinding')
	end
end
local mark
mark = function(self, mark)
	return self.scene.bidons.context.marks[self.script][mark]
end
local mark_out
mark_out = function(self, mark)
	return self.scene.bidons.context.marks[self.script][mark] - 1
end
local set_mark
set_mark = function(self, mark, value)
	value = value and value or true
	self.scene.bidons.context.marks[self.script][mark] = value
end
local inc_mark
inc_mark = function(self, mark, value)
	value = value and value or 1
	self.scene.bidons.context.marks[self.script][mark] = self.scene.bidons.context.marks[self.script][mark] + value
end
local dec_mark
dec_mark = function(self, mark, value)
	value = value and value or 1
	self.scene.bidons.context.marks[self.script][mark] = self.scene.bidons.context.marks[self.script][mark] - value
end
return {
	background = background,
	condition = condition,
	close = close,
	clothes = clothes,
	clothes_default = clothes_default,
	emotion = emotion,
	emotion_default = emotion_default,
	['goto'] = _goto,
	["return"] = _return,
	set_char = set_char,
	menu_mode = menu_mode,
	nothing = nothing,
	shortcut = shortcut,
	shortcut2 = shortcut2,
	sprite = sprite,
	sprite_off = sprite_off,
	scene = scene,
	update = update,
	virtual_bnf = virtual_bnf,
	mark = mark,
	set_mark = set_mark,
	inc_mark = inc_mark,
	dec_mark = dec_mark
}
