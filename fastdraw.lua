local lume = require 'lume'

local fast_draw = {}

--fast_draw.anchor_x = 0
--fast_draw.anchor_y = 0

--fast_draw.anchor = function(anchor_x, anchor_y)
--	fast_draw.anchor_x = anchor_x
--	fast_draw.anchor_y = anchor_y
--	return fast_draw
--end

fast_draw.white_alpha = function(alpha)
	love.graphics.setColor(1, 1, 1, alpha)
	return fast_draw
end

fast_draw.shader = function(shader)
	if shader then
		love.graphics.setShader(shader)
	else
		love.graphics.setShader()
	end
	return fast_draw
end

fast_draw.color = function(color, alpha)
	if color then
		if color == 'black' then
			color = '#000000' .. (alpha and alpha or "ff")
		elseif color == 'white' then
			color = '#ffffff' .. (alpha and alpha or "ff")
		end
		if not alpha then
			love.graphics.setColor(lume.color(color))
		else
			love.graphics.setColor(lume.color(color, 1, alpha))
		end
	else
		love.graphics.setColor(1, 1, 1, 1)
	end
	return fast_draw
end

fast_draw.rectangle = function(mode, position_x, position_y, width, height)
	love.graphics.rectangle(mode, position_x, position_y, width, height)
	return fast_draw
end

fast_draw.rectangle_raw = function(mode, position_x, position_y, width, height)
	love.graphics.rectangle(mode, position_x, position_y, width, height)
	return fast_draw
end

fast_draw.default_font = love.graphics.getFont()

fast_draw.font = function(font, size)
--	if font then
--		love.graphics.setFont(font)
--	else
--		love.graphics.setFont(fast_draw.default_font)
--	end
--	return fast_draw
	if not font then
		love.graphics.setFont(fast_draw.default_font)
	else
		if type(font) == "string" then
			font = assets.fonts[font](size and size or 14)
		end
		love.graphics.setFont(font)
	end
	fast_draw.current_font = font
	return fast_draw
end

fast_draw.draw = function(image, x, y, ...)
	if not image then return end

	if type(image) ~= 'table' then
		love.graphics.draw(image, x, y, ...)
	elseif image.type == 'zoom_image' then
		local width = image.cw
		local height = image.ch
		love.graphics.draw(image.image,
			x + image.ox - fast_draw.anchor_x * width,
			y + image.oy - fast_draw.anchor_y * height,
			0, image.sx, image.sy)
	end
	return fast_draw
end

fast_draw.drawimage = function(object)
	if object.image == nil then return end
	
	if object.background_size == 'contain' or object.background_size == 'singletain' then
		local width = object.image.cw
		local height = object.image.ch
		love.graphics.draw(object.image.image,
			object.absx + object.image.ox,
			object.absy + object.image.oy,
			0, object.image.sx, object.image.sy)
	elseif object.background_size == 'contain-top' then
		local width = object.image.cw
		local height = object.image.ch
		love.graphics.draw(object.image.image,
			object.absx + object.image.ox,
			object.absy,
			0, object.image.sx, object.image.sy)
	else
		love.graphics.draw(object.image, object.absx, object.absy)
	end
end

fast_draw.polygon = function(...)
	love.graphics.polygon(...)
	return fast_draw
end

fast_draw.printd = function(text, x, y)
	love.graphics.print(text, x, y)
	return fast_draw
end

fast_draw.print = function(object)
	love.graphics.print(object.text, object.absx, object.absy)
	return fast_draw
end

fast_draw.printf = function(object)
	love.graphics.printf(object.text, object.absx, object.absy, object.width)
	return fast_draw
end

fast_draw.printc = function(text, x, y)
	if text then
		local font = love.graphics.getFont()
		love.graphics.print(text, x - font:getWidth(text) / 2, y - font:getHeight() / 2)
	end
	return fast_draw
end

fast_draw.pol6x = function(style, flat_poly)
	love.graphics.polygon(style, flat_poly)
	return fast_draw
end

return fast_draw