local class = require 'middleclass'

local BeScene = require 'be_scene'
local BeUiel = class('BeUiel', BeScene)

function BeUiel:initialize(...)
	BeScene.initialize(self, ..., '.ui')
end

return BeUiel