local class = require 'middleclass'

SceneManager = class('SceneManager')

function SceneManager:initialize()
	be.gamestate.registerEvents()
	self.states = {}
end

local json = require 'engine.json'

function SceneManager:load(scene_name)
	if not self.states[scene_name] then
		local content = love.filesystem.read(scene_name)
		local scene = json.decode(content)
		self.states[scene_name] = self:init(scene)
	end
	be.gamestate.switch(self.states[scene_name])
end

function SceneManager:init(scene)
	local state = { objects = {} }
	for i, v in ipairs(scene.Objects) do
		table.insert(state.objects, self:loadElement(v))
	end
	if #state.objects == 1 then
		state.objects[1]:registerEvents(state)
	end
	return state
end

function SceneManager:loadElement(element)
	return _G['Ui' .. element.Type]:new(element)
end