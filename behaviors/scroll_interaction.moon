be = require 'bidons'
Behavior = require 'behaviors.base'
ScrollInteraction = require('middleclass')('ScrollInteraction', Behavior)

ScrollInteraction.initialize = (source, owner) =>
	Behavior.initialize(@, source, owner)
	
	@up = @new_script('up')
	@down = @new_script('down')
	
ScrollInteraction._wheelmoved = (dx, dy) =>
	-- print 'wheelmoved', dy
	if dy > 0 then @run 'up', dy
	elseif dy < 0 then @run 'down', dy
	
ScrollInteraction