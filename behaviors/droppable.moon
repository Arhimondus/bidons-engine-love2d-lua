be = require 'bidons'
Behavior = require 'behaviors.base'
Droppable = require('middleclass')('Droppable', Behavior)

Droppable.initialize = (source, owner) =>
	Behavior.initialize(@, source, owner)
	
	@dropstart = @new_script('dropstart')
	@dropcond = @new_script('dropcond')
	@dropend = @new_script('dropend')
	
Droppable._dropstart = (x, y, button) =>
	-- be.stacklog\info(@, '_dropstart')
	@run 'dropstart'

Droppable._dropcond = (x, y) =>	
	r = @run 'dropcond'
	be.stacklog\info(@, '_dropcond', r)
	r
	
Droppable._dropend = (x, y) =>
	-- be.stacklog\info(@, '_dropend')
	@run 'dropend'

Droppable