be = require 'bidons'
utils = require 'utils'
lume = require 'lume'
fd = require 'fastdraw'
moonscript = require 'moonscript'

Behavior = require('middleclass')('Behavior')

Behavior.initialize = (source, owner) =>
	@source = source
	@owner = owner
	@_name = @class.name
	@use_global_events = true

Behavior.new_script = (field) =>
	if @source[field]
		script = string.gsub(@source[field], '; ', '\n')
		moon_command = moonscript.to_lua(script)
		assert(loadstring("return function(behavior, this, params, konstabel, context, saveloader, transmanager, controller, scene, item, methods, locale) #{moon_command} end"))()
	else
		nil

Behavior.run = (script, params) =>
	if @[script]
		-- status, err = pcall () ->
		@[script](@, @owner, params, be.konstabel, be.context, be.saveloader, be.transmanager, @owner.scene.controller, @owner.scene, @owner.binding_object, @owner.methods, be.settings.locale)
		-- if err then @error err

Behavior.error = (err) =>
	message = "#{err} <- `#{@_name}\\#{@owner._name}`"
	be.stacklog\error(message)
	error(message)

Behavior.reset = () =>

Behavior.__tostring = () =>
	"#{@owner._name}.#{@_name}"
	
Behavior