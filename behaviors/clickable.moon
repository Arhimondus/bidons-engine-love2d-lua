be = require 'bidons'
utils = require 'utils'
Behavior = require 'behaviors.base'
Clickable = require('middleclass')('Clickable', Behavior)

Clickable.initialize = (source, owner) =>
	Behavior.initialize(@, source, owner)
	
	@hardmode = utils.default source.hardmode, false
	@click = @new_script('click')
	
Clickable._touch = (x, y, button) =>
	@touch_cord = { :x, :y }
	true

Clickable._untouch = (x, y) =>
	if @hardmode then
		if @touch_cord and @touch_cord.x == x and @touch_cord.y == y
			@run 'click'
			if not @owner\inside(x, y)
				love.mouse.setCursor(be.defaultCursor[1], be.defaultCursor[2], be.defaultCursor[3])
	else
		@run 'click'
		if not @owner\inside(x, y)
			love.mouse.setCursor(be.defaultCursor[1], be.defaultCursor[2], be.defaultCursor[3])
		
	@touch_cord = nil
	true
	
Clickable