be = require 'bidons'
Behavior = require 'behaviors.base'
Touchable = require('middleclass')('Touchable', Behavior)

Touchable.initialize = (source, owner) =>
	Behavior.initialize(@, source, owner)

	@touch = @new_script('touch')
	
Touchable._touch = (x, y, button) =>
	@run 'touch'
	
Touchable