be = require 'bidons'
Behavior = require 'behaviors.base'
Replacable = require('middleclass')('Replacable', Behavior)

Replacable.initialize = (source, owner) =>
	Behavior.initialize(@, source, owner)

	@background_image = @new_script('background_image')
	
Replacable._gmove = (x, y, dx, dy) =>
	if @owner\inside(x, y)
		@inside = true
		love.mouse.setCursor(be.hoveredCursor[1], be.hoveredCursor[2], be.hoveredCursor[3])
		if @background_image then @owner.binding_background_image = @background_image
	else
		if @inside then
			@inside = false
			love.mouse.setCursor(be.defaultCursor[1], be.defaultCursor[2], be.defaultCursor[3])
			if @background_image
				if @owner.source.background_image
					@owner.background_image = @owner.source.background_image
				@owner.binding_background_image = @owner\new_bind 'background_image'
				@owner\updateImage()
	
Replacable