be = require 'bidons'
Behavior = require 'behaviors.base'
Hoverable = require('middleclass')('Hoverable', Behavior)

Hoverable.initialize = (source, owner) =>
	Behavior.initialize(@, source, owner)

	@background_image = @new_script('background_image')
	@background_color = @new_script('background_color')
	@color = @new_script('color')

Hoverable.reset = () =>
	love.mouse.setCursor(be.defaultCursor[1], be.defaultCursor[2], be.defaultCursor[3])
			
	if @background_image
		-- if @owner.source.background_image
		@owner.background_image = @owner.source.background_image
		@owner.binding_background_image = @owner\new_bind 'background_image'
		@owner\updateImage()
		
	if @background_color	
		-- if @owner.source.background_color
		@owner.background_color = @owner.source.background_color
		@owner.binding_background_color = @owner\new_bind 'background_color'
		
	if @color	
		-- if @owner.source.color
		@owner.color = @owner.source.color
		@owner.binding_color = @owner\new_bind 'color'

Hoverable._gmove = (x, y, dx, dy) =>
	if @owner\inside(x, y)
		@inside = true
		love.mouse.setCursor(be.hoveredCursor[1], be.hoveredCursor[2], be.hoveredCursor[3])
		if @background_image then @owner.binding_background_image = @background_image
		if @background_color then @owner.binding_background_color = @background_color
		if @color then @owner.binding_color = @color
	else
		if @inside then
			@inside = false
			@reset!

Hoverable