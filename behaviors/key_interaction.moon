be = require 'bidons'
Behavior = require 'behaviors.base'
KeyInteraction = require('middleclass')('KeyInteraction', Behavior)

KeyInteraction.initialize = (source, owner) =>
	Behavior.initialize(@, source, owner)
	@keypressed = @new_script('keypressed')
	@keyreleased = @new_script('keyreleased')
	
KeyInteraction._keypressed = (key) =>
	@run 'keypressed', key

KeyInteraction._keyreleased = (key) =>
	@run 'keyreleased', key

KeyInteraction