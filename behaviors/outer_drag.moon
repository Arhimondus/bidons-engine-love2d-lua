be = require 'bidons'
utils = require 'utils'
Behavior = require 'behaviors.base'
OuterDrag = require('middleclass')('OuterDrag', Behavior)

OuterDrag.initialize = (source, owner) =>
	Behavior.initialize(@, source, owner)
	
	@touch = @new_script('touch')
	@dragcond = @new_script('dragcond')
	@dragstart = @new_script('dragstart')
	@dragstop = @new_script('dragstop')
	@dragend = @new_script('dragend')
	@untouch = @new_script('untouch')
	@move = @new_script('move')
	
OuterDrag._touch = (x, y, button) =>
	@run 'touch'
	if @run '_dragcond'
		be.stacklog\info(@, 'DRAGCOND TRUE?')
		@run '_dragstart'

OuterDrag._dragcond = (x, y) =>
	@run 'dragcond'
	
OuterDrag._dragstart = (x, y) =>
	if not @owner.owner.owner then
		return
	
	@oldo = @owner.owner
	@loco = { @owner.absx, @owner.absy }
	
	-- be.stacklog\info(@, 'Trying to change owner to ', @owner.owner.owner._name)
	@owner\change_owner(@owner.owner.owner)
	
	@run 'dragstart'
	
	@dragging = true
	-- @owner.owner = new_owner
	
OuterDrag._dragend = (x, y) =>
	@run 'dragend', @droppable

OuterDrag._dragstop = (x, y) =>
	@owner\change_owner(@oldo)
	@owner.absolute = @loco
	@run 'dragstop'
	
OuterDrag._untouch = (x, y) =>
	@run 'untouch'
	if @droppable
		if @droppable\inside(x, y) and @droppable.behaviors.droppable\_dropcond(x, y)
			@droppable.behaviors.droppable\_dropend(x, y)
			@run '_dragend', x, y
		else
			if @dragging then @run '_dragstop', x, y
		@droppable = nil
		@oldo = nil
		@loco = nil
		@dragging = false
	else
		if @dragging then @run '_dragstop', x, y
		@droppable = nil
		@oldo = nil
		@loco = nil
		@dragging = false
	
OuterDrag._gmove = (x, y, dx, dy) =>
	count = 0
	if @dragging
		-- be.stacklog\info(@, 'move touch', @is_touch)
		@owner.xcord += dx
		@owner.ycord += dy
	
		for key, value in pairs @owner.scene.raw_list
			if value.behaviors and value.behaviors.droppable
				if value\inside(x, y)
					count += 1
					value.behaviors.droppable\_dropstart(x, y)
					@droppable = value
					print "Count = #{count}"

OuterDrag