be = require 'bidons'
Behavior = require 'behaviors.base'
LocalDrag = require('middleclass')('LocalDrag', Behavior)

LocalDrag.initialize = (source, owner) =>
	Behavior.initialize(@, source, owner)
	
	@click = @new_script('click')
	
LocalDrag._touch = (x, y, button) =>
	@is_touch = true
	@touch_cord = { :x, :y }
	@run 'touch'
	true

LocalDrag._dragcond = (x, y) =>
	if @run 'drag'
		@event 'dragstart'
	
LocalDrag._dragstart = (x, y) =>
LocalDrag._dropstart = (x, y) =>
	@is_dropstart = true
	
LocalDrag._untouch = (x, y) =>
	@is_touch = false
	@run 'untouch'
	if @touch_cord and @touch_cord.x == x and @touch_cord.y == y then @run 'click'
	@touch_cord = nil
	
LocalDrag._move = (x, y, dx, dy) =>
	be.stacklog\info(@, 'move touch', @is_touch)
	if @is_touch
		@owner.xcord += dx
		@owner.ycord += dy

LocalDrag