local be = require 'bidons'

local class = require 'middleclass'

local BeState = class('BeState')

function BeState:initialize(state_name)
	local chunk = love.filesystem.load(state_name .. '.lua')
	local state = chunk()
	for key, value in pairs(state) do
		self[key] = value
	end
	self.name = state_name
end

function BeState:enter(prev)
	be.gamestate.current().prev_state = prev
end

function BeState:draw()
end

function BeState:update(dt)
end

function BeState:preserve()
end

function BeState:restore()
end

return BeState

