be = require 'bidons'
fd = require 'fastdraw'
lume = require 'lume'
utils = require 'utils'
moonscript = require 'moonscript'
utf8 = require 'utf8'

ElBase = require 'elements.ne-base'

--@print(string.gsub("@Player.disable_avatar!", '@(%a+)%.', 'shortcut("%1").'))
--@print(string.gsub("@Player = 250", '@(%a+)%s?=%s?(.+)', 'shortcut("%1", %2).'))

build_array = (...) ->
	arr = {}
	for v in ... do
		arr[#arr + 1] = v
	arr

utf8_sub = (str, i, j) ->
	i = utf8.offset(str, i)
	if j
		o = utf8.offset(str, j + 1)
		if o
			j = o - 1
			string.sub(str, i, j)
	else
		string.sub(str, i)

ElDialog = require('middleclass')('ElDialog', ElBase)

ElDialog.initialize = (scene, source, base, binding_object, binding_index, owner) =>
	ElBase.initialize(self, scene, source, base, binding_object, binding_index, owner)
	
	-- @functions = moonscript.moon_loader('bdln-functions')()
	@functions = require 'bdln-functions'
	
	@debug_count = 0

	@script = source.script
	@char = ''
	@message = ''
	
	@marks = {}
	
	@menu = {}
	
	if not be.context.marks then
		be.context.marks = {}
	
	if @source.packed then
		return
		
	if @script then
		@load_script(@script)
		@next()

ElDialog.print = (msg) =>
	print(msg)

ElDialog.dynamic = () =>
	false

ElDialog.get_line = (position) =>
	position = position and position or @position
	if position <= 0
		position = 1
		@position = 1
	if position <= #@lines then
		return @lines[position]\trim()
	else
		return nil

ElDialog.apply_menu_variant = () =>

ElDialog.get_action_block = (position) =>
	start_pos = position
	cur_pos = position
	line = @get_line(start_pos)
	if line\last() ~= '{' then error 'Tryning to get block from non-block line! No `{`.' 
	
	bracket_count = 1
	
	while bracket_count ~= 0 do
		cur_pos = cur_pos + 1
		line = @get_line(cur_pos)
		if line\last() == '{' then
			bracket_count = bracket_count + 1
		elseif line == '}' then
			bracket_count = bracket_count - 1

	end_pos = cur_pos
	start_pos, end_pos

ElDialog.get_conditional_block = (position) =>
	start_pos = position
	cur_pos = position
	line = @get_line(start_pos)
	if not line\starts_with('if') then error 'Tryning to get block from non-conditional-block line! No `if`.' 

	else_pos = nil
	bracket_count = 1

	while bracket_count ~= 0 do
		cur_pos = cur_pos + 1
		line = @get_line(cur_pos)
		if line\ends_with('then') then
			bracket_count = bracket_count + 1
		elseif line == 'end' then
			bracket_count = bracket_count - 1
		elseif line == 'else' then
			if bracket_count == 1 then
				else_pos = cur_pos

	end_pos = cur_pos
	start_pos, end_pos, else_pos

ElDialog.run_command = (line, as_lua) =>
	if line\starts_with('return')
		moon_command = moonscript.to_lua(line)
		func = assert(loadstring('return function(dialog, functions, be) ' .. moon_command .. ' end'))
		return func()(self, @functions, @scene.bidons)

	first_sign = line\first()
	last_sign = line\last()
	
	-- Добавить %
	
	shortcut_prefix = @functions.shortcut(self)
	shortcut2_prefix = @functions.shortcut2(self)
	
	line = line\gsub('@', shortcut_prefix)
	line = line\gsub('&', shortcut2_prefix)

	for key, value in pairs @functions
		if line\starts_with(key .. '!')
			line = line\gsub(key .. '!', 'functions.' .. key .. ' dialog')
		elseif line\starts_with(key)
			line = line\gsub(key, 'functions.' .. key .. ' dialog, ')


	line = line\gsub('; scene', '\nfunctions.scene dialog, ')
	line = line\gsub('; goto', '\nfunctions.goto dialog, ')
	line = line\gsub('; ', '\n')

	moon_command = moonscript.to_lua(line)
	if moon_command == nil
		error('\r\nОшибка в строке ' .. @position .. ', скрипт ' .. @script .. '\\r\n' .. line)

	func = assert(loadstring('return function(dialog, functions) ' .. moon_command .. ' end'))
	func()(self, @functions)

ElDialog.process_menu = (line, inner_position) =>
	@stop!

	@type = 'menu'
		
	if not @menu
		@menu = {}
	
	menu = {}
	
	menu_line = line\skip(1)
	menu_line_split = utils.split_full(menu_line, " => ")
	
	menu_title = @get_message("/" .. menu_line_split[1])
	menu_action = menu_line_split[2]
	
	id = @line_ids and @line_ids[inner_position] or nil -- После нужно сделать детект id из /***/
	menu_position = inner_position
	
	menu_title = string.gsub(menu_title, '{context.authority}', tostring(be.context.Player.authority))
	menu_title = string.gsub(menu_title, '{context.gossip}', tostring(be.context.Player.gossip))

	menu.title = menu_title

	-- that = self
	menu.action = (that) ->
		if menu_action
			that\run_command(menu_action)
		
		that.menu = {}
		that.functions.update(that)
		that\next()

	-- inside_block_outer = ... --Найти, находится в конце последнего варианта в меню

	-- Смотрим на следующую строку
--	inner_position = @position + 1
	next_line = @get_line(inner_position)
	-- if not next_line then return end -- Последний или единственный вариант меню (?)
	
	while next_line and next_line\first() == '|' do
		prop, value = next_line\skip(1)\split_half()
		if prop == 'action' then
			if value\last() == '{' then
				block_start, block_end = @get_action_block(inner_position) -- Найти, с помощью счётчика скобок
				-- that = self
				menu.action = (that) ->
					@functions.inc_mark(self, 'menuclicks_' .. id, 1)
					that.menu = {}
					that.functions.update(that)
					that.inside_block = true
					that.inside_block_start = block_start
					that.inside_block_end = block_end
--						that\next()
				
				inner_position = block_end + 1
			
		elseif prop == 'if' then
			shortcut_prefix = @functions.shortcut(self)
			shortcut2_prefix = @functions.shortcut2(self)
			
			new_value = value\gsub('@', shortcut_prefix)
			new_value = new_value\gsub('&', shortcut2_prefix)

			moon_command = moonscript.to_lua(new_value)
			menu._convalue = value
			menu[prop] = assert(loadstring('return function(functions, dialog, be, shows, clicks, context)' .. moon_command .. ' end'))()
			-- (@functions, self, be, shows, clicks)
			inner_position = inner_position + 1
		else
			menu[prop] = value
			inner_position = inner_position + 1
		
		next_line = @get_line(inner_position)
	
	
	if not id then -- Следовательно ID ещё нету
		id = love.timer.getTime()
		@functions.set_mark(self, 'menushows_' .. id, 0)
		@functions.set_mark(self, 'menuclicks_' .. id, 0)
		if not @line_ids then @line_ids = {}
		@line_ids[menu_position] = id
	
	@functions.inc_mark(self, 'menushows_' .. id, 1)
	shows = @functions.mark(self, 'menushows_' .. id, 0)
	clicks = @functions.mark(self, 'menuclicks_' .. id, 0)
	if not menu['if'] or menu['if'](@functions, self, be, shows, clicks, be.context) then
		table.insert(@menu, menu)
	
	if next_line and next_line\first() == '/' then
		@position = inner_position - 1
		-- @next()
		@process_menu(next_line, inner_position + 1)
	else
		@inside_block_outer = inner_position
		@clear_menu = true
		@functions.update(self)

ElDialog.next = () =>
	-- @inside_block_start = ...
	-- @inside_block_end = ...
	-- @inside_block_outer = ...
	
	if not @script then return
	if #@menu > 0 then return

	if @disable_next_command
		@disable_next_command = false
		return
	
--	if @clear_menu then
--		@clear_menu = nil
--		@menu = {}
--		@functions.update(self)
	
	if @inside_block
		if @position <= @inside_block_start or @position >= @inside_block_end
			@position = @inside_block_start
	
	@position = @position + 1
	
	if @inside_block and @position == @inside_block_end
		@position = @inside_block_outer
		@inside_block = false
	
	-- Проверить что файл не завершился
	
	line = @get_line()
	if line == '' -- Значит пустая строка
		@print('#' .. @position .. ' empty line')
		return @next()
	
	if line == nil -- Файл закончился?
		return
	
	@process_bdlogic(line)

ElDialog.get_message = (line) =>
	if be.settings.locale_tx == 'ru'
		line = line\gsub("%/%w+%/", "")
		line
	else
		id = line\match("%/(%w+)%/")
		return be.transmanager\get_tx(id)	

ElDialog.process_bdlogic = (line) =>
	first_sign = line\first()
	last_sign = line\last()
	
	@debug_count = @debug_count + 1
	
	@custom_locale = true
	
	if first_sign == '-'
		@print('#' .. @position .. ' speach --phrase')
		@message = @get_message(line\skip(1))
		@type = 'speach'
		@mod = 'phrase'
	elseif first_sign == '*'
		@print('#' .. @position .. ' speach --thought')
		@message = @get_message(line\skip(1))
		@type = 'speach'
		@mod = 'thought'
	elseif first_sign == '/'
		@print('#' .. @position .. ' menu')
		@process_menu(line, @position + 1)
	elseif first_sign == '#'
		@print('#' .. @position .. ' mark/label')
		mark = line\skip(1)\trim()
		if not @marks[mark]
			@marks[mark] = @position
			if not be.context.marks[@script]
				be.context.marks[@script] = {}
			if not be.context.marks[@script][mark]
				be.context.marks[@script][mark] = 1
			else
				be.context.marks[@script][mark] = be.context.marks[@script][mark] + 1
		@next()
	elseif last_sign == ':'
		@print('#' .. @position .. ' character change')
		char = line\take(line\len() - 1)
		@char = be.transmanager\get_from_bdl(char)
		@char_bdl = char
		@next()
	elseif first_sign == ';' -- Значит комментарий
		@print('#' .. @position .. ' comment')
		@next()
	elseif line\starts_with('if')
		@print('#' .. @position .. ' if')
		if not line\ends_with('then')
			-- Значит инлайн вариант
			condition, action = line\split_half(' then ')
			condition = 'return ' .. condition\gsub('if ', '')
			result = @run_command(condition, true)
			if result then
				@process_bdlogic(action)
			else
				@next()
		else
			-- Иначе блочный вариант, необходимо начать поиск позиций else и end
			condition, action = line\split_half(' then')
			-- condition = 'return ' .. condition\gsub('if ', '')
			condition = condition\gsub('if ', '')
			result = @run_command(condition, true)
			-- Ищем else или end блок
			start_pos, end_pos, else_pos = @get_conditional_block(@position)
			if result
				@next()
			else
				if else_pos
					@position = else_pos
				else
					@position = end_pos
				@next()
	elseif line\starts_with('else')
		@print('#' .. @position .. ' else')
		@next()
	elseif line\starts_with('end')
		@print('#' .. @position .. ' else')
		@next()
	else -- Иначе это, вероятно, команда
		@print('#' .. @position .. ' command ' .. line)
		@run_command(line, first_sign, last_sign)
		@next()

ElDialog.end_dialog = () =>
	be.stacklog\info('Dialog ending...')
	
	@position = 0
	@menu = {}
	@disable_next_command = true
	@char = ''
	@message = ''
	@skipping = false

	for key, value in pairs(@scene.raw_list) do
		if value.behaviors
			if value.behaviors.hoverable
				value.behaviors.hoverable\reset()

ElDialog.load_script = (script) =>
	jump = nil
	if type(script) == 'table'
		jump = script[2]
		script = script[1]
	
	if not be.context.marks[script]
		be.context.marks[script] = {}
	
	@disable_next_command = false
	@script = script
	
	@line_ids = {}
	@lines = build_array(love.filesystem.lines(script .. '.bdln'))
	
	@position = 0
	@marks = {}
	
	@menu = {}
	
	if jump
		@functions.goto(@, jump)

	@

ElDialog.raw_load_script = (script) =>
	@lines = build_array(love.filesystem.lines(script .. '.bdln'))

ElDialog.pack = () =>
	@source.script = @script
	@source.packed = true
	@source.position = @position
	@source.menu = @menu
	@source.disable_next_command = @disable_next_command
	@source.char = @char
	@source.message = @message
	@source.background = @background
	@source

ElDialog.unpack = () =>
	@script = @source.script
	@raw_load_script(@script)
	
	@char = @source.char
	@position = @source.position
	@background = @source.background
	@message = @source.message
	
	@position -= 1
	
	@next()

ElDialog.skip = () =>
	be.stacklog\info('Starting skipping...')
	
	if @skipping or #@menu > 0 then return 
	
	@skipping = true

	require('timer').script (wait) ->
		while @skipping and #@menu == 0
			@next()
			wait(0.05)

ElDialog.stop = () =>
	print 'Force stopping dialog.'
	@skipping = false
	require('timer').clear()

ElDialog