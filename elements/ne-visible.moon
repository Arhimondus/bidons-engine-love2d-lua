be = require 'bidons'
utils = require 'utils'
lume = require 'lume'
fd = require 'fastdraw'
NeBase = require 'elements.ne-base'

NeVisible = require('middleclass')('ElVisible', NeBase)

NeVisible.initialize = (scene, source, base, binding_object, binding_index, owner) =>
	NeBase.initialize(@, scene, source, base, binding_object, binding_index, owner)
	
	@visible = utils.default source.visible, true
	
	location = utils.default source.location, {0, 0}
	@xcord = location[1]
	@ycord = location[2]
	
	size = utils.default source.size, {0, 0}
	@width = size[1]
	@height = size[2]
	
	anchor = utils.default source.anchor, {0, 0}
	@anchx = anchor[1]
	@anchy = anchor[2]
	
	@color = utils.default source.color, '#ffffff'
	@opacity = source.opacity
	
	@absorb = utils.default source.absorb, true
	
	-- Список всех событий и проверочных условий
	@touch = @new_script 'touch'
	@dragcond = @new_script 'dragcond' -- Условие
	@dragstart = @new_script 'dragstart'
	@dropstart = @new_script 'dropstart'
	@untouch = @new_script 'untouch'
	@dropcond = @new_script 'dropcond' -- Условие
	@dragend = @new_script 'dragend' -- Возможно dragend, когда условие не выполнилось
	@dropend = @new_script 'dropend' -- dropend, когда условие дропа выполнилось
	
	if source.behaviors
		@behaviors = {}
		for key, value in pairs source.behaviors
			@behaviors[key] = (require 'behaviors.' .. key)(value, @)
		
NeVisible.__index = (property) =>
	switch property
		when 'location'
			{ @xcord, @ycord }
		when 'size'
			{ @width, @height }
		when 'anchor'
			{ @anchx, @anchy }
		when 'origin'
			if @owner
				utils.offset(@location, @owner.absolute)
			else
				@location
		when 'absolute'
			utils.negative_offset(@origin, { @anchx * @width, @anchy * @height })
		when 'absx'
			@absolute[1]
		when 'absy'
			@absolute[2]
		when 'orix'
			@origin[1]
		when 'oriy'
			@origin[2]
		else
			rawget(@, property)
			-- NeVisible.__index(@, property)
	
NeVisible.__newindex = (property, new_value) =>
	switch property
		when 'location'
			@xcord = new_value[1]
			@ycord = new_value[2]
		when 'size'
			@width = new_value[1]
			@height = new_value[2]
		when 'absolute'
			if @owner then
				@location = utils.negative_offset(utils.offset(new_value, { @anchx * @width, @anchy * @height }), @owner.absolute)
			else
				@location = property
		when 'absx'
			@absolute = { new_value, @absolute[2] }
		when 'absy'
			@absolute = { @absolute[1], new_value }
		else
			rawset(@, property, new_value)
			-- NeBase.__newindex(@, property, new_value)
			

NeVisible.draw = () =>
NeVisible.update = () =>

NeVisible.inside = (x, y) =>
	utils.inside_rectangle(x, y, @figure!)

NeVisible.actionable = () =>
	@visible and (@behaviors or (@objects and #@objects > 0))
	
NeVisible.figure = () =>
	@absx, @absy, @width, @height

NeVisible.event = (event_name, ...) =>
	-- be.stacklog\info(@, 'event', event_name)
	
	if @behaviors
		for _, behavior in pairs @behaviors
			if behavior['_' .. event_name]
				behavior['_' .. event_name](behavior, ...)
				
	-- if @absorb and (event_name == 'touch' or event_name == 'untouch')
		-- be.stacklog\info(@, 'absorbs', event_name)
		
	@absorb

-- touch -- dragstart
NeVisible.mousepressed = (x, y, button) =>
	@event('touch', x, y, button)
	
-- click -- dragend --dropend
NeVisible.mousereleased = (x, y) =>
	@event('untouch', x, y)

-- move -- dragging (touch + move) --dropstart -- dropping (dropstart + move)
NeVisible.mousemoved = (x, y, dx, dy) =>
	@event('move', x, y, dx, dy)

NeVisible.wheelmoved = (x, y) =>
	@event('wheelmoved', x, y)
	
NeVisible.keypressed = (key, scancode) =>
	@event('keypressed', key, scancode)

NeVisible.keyreleased = (key) =>
	@event('keyreleased', key)

NeVisible._touch = (x, y, button) =>
NeVisible._dragcond = () =>
NeVisible._dragstart = () =>
NeVisible._dropstart = () =>
NeVisible._untouch = () =>
NeVisible._dropcond = () =>
NeVisible._dragend = (dragresult) =>
NeVisible._dropend = () =>
NeVisible._move = (x, y, dx, dy) =>
		
NeVisible.drawdebug = () =>
	if @debugmode
		with fd
			.color('#ff2233')
			.font(@debugfont)
			.printd(@_name .. ' [' .. @class.name .. ']', @absx, @absy)
			-- Box
			.rectangle('line', @absx, @absy, @width, @height)
			-- Coords
			.color('#222222')
			.printd('Loc ' .. @xcord .. ':' .. @ycord, @absx + 12, @absy + 106)
			.printd('Abs ' .. @absx .. ':' .. @absy, @absx + 12, @absy + 123)
			.printd('Size ' .. @width .. ':' .. @height, @absx + 12, @absy + 140)
			.printd((@dropping == true and 'T' or 'F'), @absx + 85, @absy + 106)
			.printd('Owner ' .. @owner._name, @absx + 12, @absy + 150)
			-- Origin
			.color('#2233ff')
			.rectangle('fill', @orix - 5, @oriy - 5, 10, 10)

NeVisible.selfdestroy = () =>
	utils.remove(@owner.objects, (o) -> o == @)

NeVisible.pack = () =>
	@source

NeVisible