be = require 'bidons'
utils = require 'utils'
lume = require 'lume'
fd = require 'fastdraw'
NeBox = require 'elements.ne-box'
inspect = require 'inspect'

NeArray = require('middleclass')('ElArray', NeBox)

NeArray.initialize = (scene, source, base, binding_object, binding_index, owner) =>
	NeBox.initialize(@, scene, source, base, binding_object, binding_index, owner)
	-- be.stacklog\error("#{@_name} #{inspect.inspect(@binding_object)}")
	
	@is_object_array = utils.default source.is_object_array, false
	
	@padding = utils.default source.padding, { 0, 0, 0, 0 }
	@offset = source.offset
	
	@binding_offset = @new_bind('offset')
	@binding_padding = @new_bind('padding')
	
	-- override from NeVisible
	@absorb = utils.default source.absorb, false
	 
	-- print "absorb = #{@absorb}"
	
	if not source.template then	error 'No template element!'

	@template = source.template
		
	@binding_array = @new_bind('array')
	@array = @bind('array')
	
	@updateBinding!
	
	if @source.packed then @unpack!

NeArray.__index = (property) =>
	if property == 'offx'
		@offset[1]
	elseif property == 'offy'
		@offset[2]
	else
		NeBox.__index(@, property)
		
NeArray.update = (dt) =>
	old_offset = self.offset
	old_padding = self.padding
	@offset = @bind('offset')
	@padding = @bind('padding')
	if old_offset[1] != self.offset[1] or old_offset[2] != self.offset[2] or old_padding[1] != self.padding[1] or old_padding[2] != self.padding[2] or old_padding[3] != self.padding[3] or old_padding[4] != self.padding[4]
		@updateBinding()
	NeBox.update(@, dt)

NeArray.updateBinding = () =>
	old_count = #@objects
	
	@array = @bind('array')
	@objects = {}
	
	-- if @array == nil or #@array == 0 then
		-- if @autofit
			-- @size = {0, 0}
		-- return
	
	if not @is_object_array
		if @array
			for i, array_item in ipairs @array
				source = utils.shallow_copy(@template)
				source.location = { (i - 1) * @offx + @padding[4], (i - 1) * @offy + @padding[1] }
				newel = @scene\loadElement(source, nil, array_item, i, @)
				table.insert(@objects, newel)
				newel\update(1)
	else
		i = 1
		for key, value in pairs @array
			array_item = { :key, :value }
			source = utils.shallow_copy(@template)
			source.location = { (i - 1) * @offx + @padding[4], (i - 1) * @offy + @padding[1] }
			newel = @scene\loadElement(source, nil, array_item, i, @)
			table.insert(@objects, newel)
			newel\update(1)
			i += 1
			
	if @autofit
		max_width = 0
		max_height = 0
		
		for i, v in ipairs @objects
			potential_width = v.xcord + v.width
			potential_height = v.ycord + v.height
			if potential_width > max_width
				max_width = potential_width
			
			if potential_height > max_height
				max_height = potential_height
	
		@size = { max_width, max_height }
	
	new_count = #@objects
	
	-- print("ElArray:updateBinding [#{@_name}] from #{old_count} to #{new_count}")

NeArray.setArray = (array) =>
	@array = array
	@updateBinding()

NeArray.pack = () =>
	packed = NeBox.pack(@)
	packed.packed = true
	packed
	
NeArray.unpack = () =>
	-- NeBox.unpack(@)
	@updateBinding()

NeArray