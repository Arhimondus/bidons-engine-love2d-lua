be = require 'bidons'
utils = require 'utils'
lume = require 'lume'
fd = require 'fastdraw'
NeBox = require 'elements.ne-box'
inspect = require 'inspect'

NeGrid = require('middleclass')('ElGrid', NeBox)

NeGrid.initialize = (scene, source, base, binding_object, binding_index, owner) =>
	NeBox.initialize(@, scene, source, base, binding_object, binding_index, owner)
	-- be.stacklog\error("#{@_name} #{inspect.inspect(@binding_object)}")
	
	@is_object_array = utils.default source.is_object_array, false
	
	@capacity = utils.default source.capacity, 0
	@padding = utils.default source.padding, { 0, 0, 0, 0 }
	@grid = utils.default source.grid, { 4, 5, 5, 5, 1, 'vertical' } -- columns, rows, gapx, gapy, ratio, type

	@grid_columns = @grid[1]
	@grid_rows = @grid[2]
	@grid_gapx = @grid[3]
	@grid_gapy = @grid[4]
	@grid_ratio = @grid[5]
	@grid_type = @grid[6]
	@grid_fixed_height = utils.default source.grid_fixed_height, nil

	-- or @grid_rows, @grid_columns, @grid_gap @grid_type
	
	if not source.template then	error 'No template element!'

	@template = source.template
		
	@binding_array = @new_bind('array')
	@array = @bind('array')
	
	@run 'init'
	@updateBinding!

NeGrid.__index = (property) =>
	if property == 'offx'
		@offset[1]
	elseif property == 'offy'
		@offset[2]
	else
		NeBox.__index(@, property)

NeGrid.update = (dt) =>
	NeBox.update(@, dt)

NeGrid.getItem = (position, item) =>
	if @grid_type == 'vertical'
		width = (@width - (@grid_columns - 1) * @grid_gapx - @padding[4] - @padding[2]) / @grid_columns
		height = @grid_fixed_height and @grid_fixed_height or (width * @grid_ratio)

		source = utils.shallow_copy(@template)
		source.size = { width, height }
		
		row = math.floor((position - 1) / @grid_columns)
		column = position - row * @grid_columns - 1
		
		source.location = { column * (width + @grid_gapx) + @padding[4], row * (height + @grid_gapy) + @padding[1] }
		
		@scene\loadElement(source, nil, item, position, @)
		
NeGrid.updateBinding = () =>
	old_count = #@objects

	@array = @bind('array')
	@objects = {}

	items_count = 0
	
	if not @is_object_array
		if @array
			for i, array_item in ipairs @array
				items_count += 1
				newel = @getItem(i, array_item)
				table.insert(@objects, newel)
				newel\update(1)
	else
		i = 1
		for key, value in pairs @array
			items_count += 1
			array_item = { :key, :value }
			newel = @getItem(i, array_item)
			table.insert(@objects, newel)
			newel\update(1)
			i += 1

	for i = items_count + 1, @capacity
		newel = @getItem(i, nil)
		table.insert(@objects, newel)
		newel\update(1)

	new_count = #@objects

	-- print("ElGrid:updateBinding [#{@_name}] from #{old_count} to #{new_count}")

NeGrid.setArray = (array) =>
	@binding_array = () =>
		array
	@updateBinding!

NeGrid