be = require 'bidons'
utils = require 'utils'
lume = require 'lume'
fd = require 'fastdraw'
NeBox = require 'elements.ne-box'

NeDynamic = require('middleclass')('ElDynamic', NeBox)

NeDynamic.initialize = (scene, source, base, binding_object, binding_index, owner) =>
	NeBox.initialize(@, scene, source, base, binding_object, binding_index, owner)
	
NeDynamic\include(require 'behaviors.outer_drag')

NeDynamic