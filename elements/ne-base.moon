be = require 'bidons'
utils = require 'utils'
lume = require 'lume'
fd = require 'fastdraw'
moonscript = require 'moonscript'

UtMethods = require 'ut-methods'
NeBase = require('middleclass')('ElBase')

NeBase.initialize = (scene, source, base, binding_object, binding_index, owner) =>
	if not source.name then source.name = "name#{love.timer.getTime!}"
	
	if binding_index
		@_name = source.name .. '#' .. binding_index
	else
		@_name = source.name
	
	@binding_index = binding_index
	@vars = {}
	@owner = owner
	
	if binding_object != nil
		@binding_object = binding_object
		@arrayed_object = true
	
	@scene = scene
	
	if not @scene.raw_list[@_name]
		@scene.raw_list[@_name] = @
	-- else
		-- error "Элемент с идентификатором `#{@_name}` уже есть!"
	
	@source = source
	
	@methods = UtMethods(@)

	@debugmode = source.debugmode
	
	@debugfont = love.graphics.newFont('assets/fonts/open_sans.ttf', 12)

NeBase.actionable = () => false

NeBase.new_bind = (field) =>
	if @source['@' .. field]
		script = string.gsub(@source['@' .. field], '; ', '\n')
		moon_command = moonscript.to_lua(script)
		assert(loadstring("return function(this, scene, item, utils, fd, context, konstabel, transmanager, saveloader, settings, locale, fr) #{moon_command} end"))()
	else
		nil

NeBase.new_script = (field, default_value) =>
	if @source[field]
		script = string.gsub(@source[field], '; ', '\n')
		moon_command = moonscript.to_lua(script)
		assert(loadstring("return function(this, param, be, context, controller, scene) #{moon_command} end"))()
	else
		nil

NeBase.run = (script, ...) =>
	be.stacklog\trace(@, 'run', script)
	result = nil
	if @[script]
		-- status, err = pcall (...) ->
		result = @[script](@, ...)
		-- if err then @error(err)
	result
	
NeBase.error = (err) =>
	message = "#{err} <- `#{@_name}\\#{@class.name}`"
	be.stacklog\error(message)
	error(message)
	
NeBase.bind_str = (field) =>
	result = @bind(field)
	if result then
		tostring(result)
	else
		''

NeBase.bind = (field, smth) =>
	result = nil
	-- status, err = pcall (...) ->
	if not @['binding_' .. field]
		if not smth
			result = @[field]
		else
			return nil
	else
		result = @['binding_' .. field](@, @scene, @binding_object, utils, fd, be.context, be.konstabel, be.transmanager, be.saveloader, be.settingsWrapper, be.settings.locale, fr)
	-- if err then @error(err)
	result

NeBase.pack = () =>
	@source
	
NeBase.__tostring = () =>
	"#{@_name}"

-- NeBase.__index = (property) =>
	-- @vars[property]

-- NeBase.__newindex = (property, new_value) =>
	-- @vars[property] = new_value

NeBase