utils = require 'utils'
lume = require 'lume'
fd = require 'fastdraw'
NeVisible = require 'elements.ne-visible'

NeLabel = require('middleclass')('ElLabel', NeVisible)

NeLabel.initialize = (scene, source, base, binding_object, binding_index, owner) =>
	NeVisible.initialize(@, scene, source, base, binding_object, binding_index, owner)
	
	@init = @new_script 'init'
	
	@color = source.color
	@opacity = utils.default source.opacity, 1
	
	size, font = unpack(source.font)
	size = string.gsub(size, 'pt', '')
	@_fsize = size
	@_fpath = font
	@font = love.graphics.newFont(font, tonumber(size))

	@text = source.text
	
	@width = nil
	@height = nil
	
	@background_color = source.background_color
	
	@binding_font = @new_bind('font')
	@binding_text = @new_bind('text')
	@binding_color = @new_bind('color')
	@binding_visible = @new_bind('visible')
	@binding_opacity = @new_bind('opacity')
	@binding_background_color = @new_bind 'background_color'
	
	@run 'init'
	
NeLabel.update = (dt) =>
	new_font = @bind('font', true)
	if new_font
		size = new_font[1]
		path = new_font[2]
		if size != @_fsize or path != @_fpath
			@font = love.graphics.newFont(path, size)

	new_text = @bind_str('text')
	if @text != new_text
		@text = @bind_str('text')
		-- @updateSize()
		
	@opacity = @bind('opacity')
	@color = @bind('color')
	@visible = @bind('visible')

NeLabel.draw = () =>
	if not @visible then return
	
	if @pre_draw
		@pre_draw(@binding_object, utils, fd)
	
	if @background_color
			with fd
				.color(@bind('background_color'), @opacity)
				.rectangle('fill', @absx, @absy, @width, @height)

	if @border
		with fd
			.color('black')
			.rectangle('line', @figure())
	
	with fd
		.font(@font)
		.color(@color, @opacity)

	if @text and #@text > 0
		fd.print(@)
	
	@drawdebug()
	
NeLabel.__index = (property) =>
	if property == 'width'
		if @text and @text != '' then @font\getWidth(@text)
		else 0
	elseif property == 'height'
		@font\getHeight()
	else
		NeVisible.__index(@, property)

NeLabel