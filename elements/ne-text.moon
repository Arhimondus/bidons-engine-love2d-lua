utils = require 'utils'
lume = require 'lume'
fd = require 'fastdraw'
NeVisible = require 'elements.ne-visible'

NeText = require('middleclass')('ElText', NeVisible)

NeText.initialize = (scene, source, base, binding_object, binding_index, owner) =>
	NeVisible.initialize(@, scene, source, base, binding_object, binding_index, owner)

	@init = @new_script 'init'
	@color = source.color
	@opacity = utils.default source.opacity, 1
	@background_color = source.background_color
	
	size, font = unpack(source.font)
	size = string.gsub(size, 'pt', '')
	@_fsize = size
	@_fpath = font
	@font = love.graphics.newFont(font, tonumber(size))

	@text = source.text

	@binding_font = @new_bind('font')
	@binding_text = @new_bind('text')
	@binding_color = @new_bind('color')
	@binding_visible = @new_bind('visible')
	@binding_background_color = @new_bind('background_color')
	
	@run 'init'

NeText.update = (dt) =>
	new_font = @bind('font', true)
	if new_font
		size = new_font[1]
		path = new_font[2]
		if size != @_fsize or path != @_fpath
			@font = love.graphics.newFont(path, size)

	new_text = @bind_str('text')
	if @text != new_text
		@text = @bind_str('text')
		-- @updateSize()
	
	@color = @bind('color')
	@visible = @bind('visible')
	@background_color = @bind('background_color')

NeText.draw = () =>
	if not @visible then return

	if @pre_draw
		@pre_draw(@binding_object, utils, fd)

	if @background_color
		with fd
			.color(@background_color, @opacity)
			.rectangle('fill', @absx, @absy, @width, @height)

	if @border
		with fd
			.color('black')
			.rectangle('line', @figure())

	with fd
		.font(@font)
		.color(@color, @opacity)

	if @text and #@text > 0
		fd.printf(@)

	@drawdebug()

NeText