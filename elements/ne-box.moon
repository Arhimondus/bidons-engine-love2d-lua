be = require 'bidons'
utils = require 'utils'
lume = require 'lume'
fd = require 'fastdraw'
NeVisible = require 'elements.ne-visible'

NeBox = require('middleclass')('ElBox', NeVisible)

NeBox.initialize = (scene, source, base, binding_object, binding_index, owner) =>
	NeVisible.initialize(@, scene, source, base, binding_object, binding_index, owner)
	
	@autofit = source.autofit
	@clip = utils.default source.clip, false
	
	-- Используется для того, чтобы определить в какой контейнер разместить временно перемещаемый элемент,
	-- вероятно сам элемент так же остаётся на том же месте в изначальном контейнере, но ему проставляется dynamic: false
	@drag_owner = @new_script 'drag_owner'
	
	@background_size = utils.default source.background_size, 'contain'
	
	if source.background_image
		@background_image = source.background_image
		@updateImage()
	
	@background_color = source.background_color
	
	@init = @new_script 'init'
	@hover = @new_script 'hover'
	@release = @new_script 'release'
	@click = @new_script 'click'
	@predraw = @new_script 'predraw'
	
	@binding_color = @new_bind 'color'
	@binding_visible = @new_bind 'visible'
	@binding_background_color = @new_bind 'background_color'
	@binding_background_image = @new_bind 'background_image'
	@binding_xobject = @new_bind 'xobject'
	
	if @binding_background_image and @background_size == 'contain' and @background_size == 'contain-top'
		@updateImage()

	if @background_size == 'singletain'
		@s_width = @width
		@s_height = @height
		@image = utils.imageZoom(@background_image, @s_width, @s_height)
		
	@temporary_owner = nil
	
	@objects = {}
	
	if not @arrayed_object
		-- be.stacklog\info 'xobject'
		new_bind = @bind('xobject')
		if new_bind
			@binding_object = new_bind
		else
			@binding_object = binding_object

	if source.objects
		for _, obj in ipairs source.objects
			newel = @scene\loadElement(obj, @absolute, @binding_object, nil, @)
			table.insert(@objects, newel)
			
	@loadStencilFunction!
	
	@run 'init', nil, be, be.context, @scene.controller, @scene
	
NeBox.change_owner = (new_owner) =>
	absolute = @absolute
	old_owner = @owner
	new_owner\add(@)
	utils.remove(old_owner.objects, (o) -> o == @)
	@absolute = absolute
	
NeBox.add = (ui_object) =>
	table.insert(@objects, ui_object)
	ui_object.owner = @

NeBox.updateImage = () =>
	if type(@background_image) == 'string'
		path = @background_image\gsub('{locale}', be.settings.locale)
		@image = love.graphics.newImage(path)
	else
		@image = @background_image
		
	if (@background_size == 'contain' or @background_size == 'contain-top') and @image then
		@image = utils.imageZoom(@image, @width, @height)
	elseif @background_size == 'singletain' and @image then
		@image = utils.imageZoom(@image, @s_width, @s_height)

NeBox.update = (dt) =>
	if not @arrayed_object
		@binding_object = @bind('xobject')
	
	@color = @bind('color')
	@visible = @bind('visible')
	@background_color = @bind('background_color')

	old_background = @background_image
	@background_image = @bind('background_image')
	if old_background != @background_image
		@updateImage()
		
	for _, v in ipairs @objects
		v\run('update', dt)
	
NeBox.basedraw = () =>
	if not @visible then return
	
	if @pre_draw then
		@pre_draw(be, @binding_object, utils, fd)
	
	if @is_hovered and @shader
		with fd
			.shader(@shader)
			.drawimage(@)
			.shader()
	else
		if @background_color
			with fd
				.color(@bind('background_color'), @opacity)
				.rectangle('fill', @absx, @absy, @width, @height)
	
		with fd
			.color(@color, @opacity)
			.drawimage(@)
	
	if @after_draw
		@after_draw(be, @binding_object, utils, fd)
	
	for _, v in ipairs @objects
		v\run('draw')

NeBox.loadStencilFunction = () =>
	that = @
	
	@stencilFunciton = () ->
		with fd
			.color()
			.rectangle("fill", that.absx, that.absy, that.width, that.height)

NeBox.draw = () =>
	if @clip
		with love.graphics
			.stencil(@stencilFunciton, "replace", 1)
			.setStencilTest("greater", 0)
		
		@basedraw()
		
		love.graphics.setStencilTest()
	else
		@basedraw()

	@drawdebug()

NeBox.mousepressed = (x, y, button) =>
	-- be.stacklog\info(@, 'mousepressed', { x, y, button })
	
	for _, v in lume.ripairs(@objects)
		if v\actionable! and v\run 'inside', x, y
			if v\run 'mousepressed', x, y then return true
	
	NeVisible.mousepressed(@, x, y, button)

NeBox.mousemoved = (x, y, dx, dy) =>
	be.stacklog\debug(@, 'mousemoved')

	for _, v in lume.ripairs(@objects)
		if v\actionable! and v\run 'inside', x, y, dx, dy
			if v\run 'mousemoved', x, y, dx, dy 
				return true

	NeVisible.mousemoved(@, x, y, dx, dy)
			
NeBox.mousereleased = (x, y) =>
	-- be.stacklog\info(@, 'mousereleased', { x, y })
	
	for _, v in lume.ripairs(@objects)
		if v\actionable! and v\run 'inside', x, y
			if v\run 'mousereleased', x, y then return true
	
	NeVisible.mousereleased(@, x, y)

NeBox.keypressed = (key, scancode) =>
	for _, v in lume.ripairs(@objects)
		v\run 'keypressed', key, scancode
	
	NeVisible.keypressed(@, key, scancode)
		
NeBox.keyreleased = (key) =>
	for _, v in lume.ripairs(@objects)
		v\run 'keyreleased', key
	
	NeVisible.keyreleased(@, key)
		
NeBox.wheelmoved = (x, y) =>
	for _, v in lume.ripairs(@objects)
		v\run 'wheelmoved', x, y
		
	NeVisible.wheelmoved(@, x, y)
	
NeBox.pack = () =>
	packed_container = NeVisible.pack(@)
	packed_container.background_image = @bind('background_image')
	packed_container.objects = {}
	for _, v in ipairs self.objects
		table.insert(packed_container.objects, v\pack!)
	return packed_container 

NeBox