local be = require 'bidons'

function test_addition()
	be.context = {}
	
	assert_equal(2 + 2, 4)
end