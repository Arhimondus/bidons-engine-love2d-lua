local be = require 'bidons'
local lume = require 'lume'

function test_box_simple()
	be.konstabel:entrust('simple_box')
	local image = love.graphics.captureScreenshot(function(image)
		r, g, b, a = image:getPixel(200, 250)
		color = lume.color(be.gamestate.current:get('simple_box_ctl').background_color)
		assert_equal({ r, g, b, a }, color)
	end)
end

function test_subtraction()
  assert_equal(2 - 2, 0)
end