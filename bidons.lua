local be = {}

local utf8 = require "utf8"
local utils = require "utils"
 
local function error_printer(msg, layer)
	print((debug.traceback("Error: " .. tostring(msg), 1+(layer or 1)):gsub("\n[^\n]+$", "")))
end
 
function love.errorhandler(msg)
	msg = tostring(msg)
 
	error_printer(msg, 2)
 
	if not love.window or not love.graphics or not love.event then
		return
	end
 
	if not love.graphics.isCreated() or not love.window.isOpen() then
		local success, status = pcall(love.window.setMode, 800, 600)
		if not success or not status then
			return
		end
	end
 
	-- Reset state.
	if love.mouse then
		love.mouse.setVisible(true)
		love.mouse.setGrabbed(false)
		love.mouse.setRelativeMode(false)
		if love.mouse.isCursorSupported() then
			love.mouse.setCursor()
		end
	end
	if love.joystick then
		-- Stop all joystick vibrations.
		for i,v in ipairs(love.joystick.getJoysticks()) do
			v:setVibration()
		end
	end
	if love.audio then love.audio.stop() end
 
	love.graphics.reset()
	
	local font = love.graphics.setNewFont(be.defaultFont, 14)
 
	love.graphics.setColor(1, 1, 1, 1)
 
	local trace = debug.traceback()
 
	love.graphics.origin()
 
	local sanitizedmsg = {}
	for char in msg:gmatch(utf8.charpattern) do
		table.insert(sanitizedmsg, char)
	end
	sanitizedmsg = table.concat(sanitizedmsg)
 
	local err = {}
 
	table.insert(err, "Error\n")
	table.insert(err, sanitizedmsg)
 
	if #sanitizedmsg ~= #msg then
		table.insert(err, "Invalid UTF-8 string in error message.")
	end
 
	table.insert(err, "\n")
 
	for l in trace:gmatch("(.-)\n") do
		if not l:match("boot.lua") then
			l = l:gsub("stack traceback:", "Traceback\n")
			table.insert(err, l)
		end
	end
 
	local p = table.concat(err, "\n")
 
	p = p:gsub("\t", "")
	p = p:gsub("%[string \"(.-)\"%]", "%1")
 
	local function draw()
		local pos = 70
		love.graphics.clear(89/255, 157/255, 220/255)
		love.graphics.printf(p, pos, pos, love.graphics.getWidth() - pos)
		love.graphics.present()
	end
 
	local fullErrorText = p
	local function copyToClipboard()
		if not love.system then return end
		love.system.setClipboardText(fullErrorText)
		p = p .. "\nCopied to clipboard!"
		draw()
	end
 
	if love.system then
		p = p .. "\n\nPress Ctrl+C or tap to copy this error"
	end
 
	be.utilitaryChannel:push('error ' .. msg)
	
	return function()
		love.event.pump()
 
		for e, a, b, c in love.event.poll() do
			if e == "quit" then
				return 1
			elseif e == "keypressed" and a == "escape" then
				return 1
			elseif e == "keypressed" and a == "c" and love.keyboard.isDown("lctrl", "rctrl") then
				copyToClipboard()
			elseif e == "touchpressed" then
				local name = love.window.getTitle()
				if #name == 0 or name == "Untitled" then name = "Game" end
				local buttons = {"OK", "Cancel"}
				if love.system then
					buttons[3] = "Copy to clipboard"
				end
				local pressed = love.window.showMessageBox("Quit "..name.."?", "", buttons)
				if pressed == 1 then
					return 1
				elseif pressed == 3 then
					copyToClipboard()
				end
			end
		end
 
		draw()
 
		if love.timer then
			love.timer.sleep(0.1)
		end
	end
end

be.setup = function(command_args, settings_default, sl_classes)
	require 'extend_string'
	require 'lualinq'
	
	local BeKonstabel = require 'be_konstabel'
	local BeSaveloader = require 'be_saveloader'
	local BeTransmanager = require 'be_transmanager'
	local BeStacklog = require 'be_stacklog'
	
	local lume = require 'lume'

	_G['GBIDONS'] = be
	
	local settings_option = lume.find(command_args, '--settings')
	local settings_path
	if settings_option then
		settings_path = command_args[settings_option + 1]
	else
		settings_path = 'settings.json'
	end
	
	local settings_file = love.filesystem.read(settings_path)
	local settings
	if settings_file then
		settings = require('json').decode(settings_file)
	else
		if not settings_default then
			settings = {
				enable_auto_skip = false,
				skip_after_choice = false,
				skip_intro = false,
				auto_player_name = false,
				borderless = false,
				locale = 'en',
				locale_tx = 'en',
				locales = { 'en', 'ru', 'de', 'es', 'fr', 'it', 'pt', 'tr' },
				locales_tx = { 'en', 'ru', 'de', 'es', 'fr', 'it', 'pt', 'tr' },
				fullscreen = false,
				display = 1
			}
		else
			settings = settings_default
		end
	end
	
	be.timer = require 'timer'
	be.context = nil
	be.konstabel = BeKonstabel:new()
	be.saveloader = BeSaveloader:new(sl_classes)
	be.transmanager = BeTransmanager:new(settings.locale, settings.locale_tx)
	be.settings = settings
	be.settingsWrapper = function()
		local settings = be.settings
		local settingsWrapper = {}
		local saveSettings = function(settings)
			local data = require('json').encode(settings)
			love.filesystem.write(settings_path, data)
		end
		table.insert(settingsWrapper, {
			settings = settings,
			name = 'fullscreen',
			value = settings.fullscreen,
			needrestart = false,
			apply = function(stwv)
				love.window.setFullscreen(stwv.settings.fullscreen, 'exclusive')
			end,
			change = function(stwv)
				stwv.settings.fullscreen = not stwv.value
				stwv.value = stwv.settings.fullscreen
				saveSettings(stwv.settings)
				stwv:apply()
			end
		})
		table.insert(settingsWrapper, {
			settings = settings,
			name = 'borderless',
			value = settings.borderless,
			disabled = false,
			needrestart = true,
			apply = function(stwv)
				local width, height, flags = love.window.getMode()
				flags.borderless = stwv.settings.borderless
				love.window.setMode(width, height, { borderless = stwv.settings.borderless })
			end,
			change = function(stwv)
				stwv.settings.borderless = not stwv.value
				saveSettings(stwv.settings)
				be.utilitaryChannel:push('kill')
				be.utilitaryThread:wait( )
				love.event.quit('restart')
			end
		})
		table.insert(settingsWrapper, {
			settings = settings,
			name = "display",
			value = settings.display,
			disabled = false,
			needrestart = true,
			apply = function(stwv)
				local width, height, flags = love.window.getMode()
				flags.display = stwv.settings.display
				love.window.setMode(width, height, flags)
			end,
			change = function(stwv)
				local displaysCount = love.window.getDisplayCount()
				stwv.settings.display = stwv.value + 1
				if stwv.settings.display > displaysCount then
					stwv.settings.display = 1
				end
				saveSettings(stwv.settings)
				be.utilitaryChannel:push('kill')
				be.utilitaryThread:wait( )
				love.event.quit('restart')
			end
		})
		table.insert(settingsWrapper, {
			settings = settings,
			name = "uilang",
			value = settings.locale,
			needrestart = false,
			apply = function(stwv)
				be.transmanager = BeTransmanager:new(stwv.settings.locale, stwv.settings.locale_tx)
			end,
			change = function(stwv)
				local index = utils.findFirstIndex(stwv.settings.locales, function(e) return e == stwv.value end)
				if index >= #stwv.settings.locales then
					index = 1
				else
					index = index + 1
				end
				stwv.settings.locale = stwv.settings.locales[index]
				stwv.settings.locale_tx = stwv.settings.locales_tx[index]
				stwv.value = stwv.settings.locale
				saveSettings(stwv.settings)
				stwv:apply()
			end
		})
		return settingsWrapper
	end
	
	local currentSettings = be.settingsWrapper()
	for i, v in ipairs(currentSettings) do
		v:apply()
	end

	be.gamestate = require 'gamestate'
	be.stacklog = BeStacklog:new()
	
	be.gamestate.registerEvents()
	
	be.version = 0.50
	
	return be
end

be.extend_elements = function(extended_objects)
	local elements = require 'elements'
	for key, value in pairs(extended_objects) do
		elements[key] = value
	end
	
	return be
end

return be