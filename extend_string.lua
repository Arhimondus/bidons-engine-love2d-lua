local utf8 = require 'utf8'

local function utf8_sub(str, x, y)
	local x2, y2
	x2 = utf8.offset(str, x)
	if y then
		y2 = utf8.offset(str, y + 1)
		if y2 then
			y2 = y2 - 1
		end
	end
	if not x2 then return false end
	return string.sub(str, x2, y2)
end

function string:first()
	return utf8_sub(self, 1, 1)
end

function string:last()
	return utf8_sub(self, -1)
end

function string:take(number)
	return utf8_sub(self, 1, number)
end

function string:skip(number)
	local len = utf8.len(self)
	return utf8_sub(self, -len + number)
end

function string:len()
	local len = utf8.len(self)
	return len
end

function string:split_get(sep, num, default)
	local part = self:split(sep, plain)[num]
	if part then
		return part
	else
		return default and default or nil
	end
end

function string:split(sep, plain)
	-- from https://github.com/moteus/lua-split
	
	local function is_match_empty(pat, plain)
		return not not string.find('', pat, nil, plain)
	end
	
	local str = self
	
	local b, res = 0, {}
	sep = sep or '%s+'

	assert(type(sep) == 'string')
	assert(type(str) == 'string')

	if #sep == 0 then
		for i = 1, #str do
			res[#res + 1] = string.sub(str, i, i)
		end
		return res
	end

	assert(not is_match_empty(sep, plain), 'delimiter can not match empty string')

	while b <= #str do
		local e, e2 = string.find(str, sep, b, plain)
		if e then
			res[#res + 1] = string.sub(str, b, e-1)
			b = e2 + 1
			if b > #str then res[#res + 1] = "" end
		else
			res[#res + 1] = string.sub(str, b)
			break
		end
	end
	return res
end

function string:split_half(sep, plain)
	-- from https://github.com/moteus/lua-split
	
	local function is_match_empty(pat, plain)
		return not not string.find('', pat, nil, plain)
	end
	
	local str = self
	
	sep = sep or '%s+'

	assert(type(sep) == 'string')
	assert(type(str) == 'string')

	if #sep == 0 then
		return string.sub(str, 1, 1), string.sub(str, 2)
	end

	assert(not is_match_empty(sep, plain), 'delimiter can not match empty string')

	local e, e2 = string.find(str, sep, nil, plain)
	if e then
		return string.sub(str, 1, e - 1), string.sub(str, e2 + 1)
	end
	return str
end

function string:split_half_end(sep, plain)
	
	local function find_from_end(str)
		for i = #str, 1, -1 do
			local char = string.sub(str, i, i)
			if char == sep then
				return i, i
			end
		end
		return -1
	end

	-- from https://github.com/moteus/lua-split
	
	local function is_match_empty(pat, plain)
		return not not string.find('', pat, nil, plain)
	end
	
	local str = self
	
	sep = sep or '%s+'

	assert(type(sep) == 'string')
	assert(type(str) == 'string')

	if #sep == 0 then
		return string.sub(str, 1, 1), string.sub(str, 2)
	end

	assert(not is_match_empty(sep, plain), 'delimiter can not match empty string')

	local e, e2 = find_from_end(self)
	if e then
		return string.sub(str, 1, e - 1), string.sub(str, e2 + 1)
	end
	return str
end

function string:contains(substr)
	if string.find(self, substr) then
		return true
	else
		return false
	end
end

function string:trim()
	local str = self:gsub('^%s+', ''):gsub('%s+$', '')
	return str
end

function string:starts_with(start)
	return start == '' or utf8_sub(self, 1, #start) == start
end

function string:ends_with(ending)
	return not self or ending == '' or utf8_sub(self, -#ending) == ending
end