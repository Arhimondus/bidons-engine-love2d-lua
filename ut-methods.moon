be = require 'bidons'
utils = require 'utils'
lume = require 'lume'
fd = require 'fastdraw'
moonscript = require 'moonscript'

UtMethods = require('middleclass')('UtMethods')

UtMethods.initialize = (owner) =>
	@owner = owner
	@methods = {}
	if owner.source.methods
		for key, value in pairs owner.source.methods
			@methods[key] = @wrap value
	
UtMethods.wrap = (field) =>
	script = string.gsub(field, '; ', '\n')
	moon_command = moonscript.to_lua(script)
	func = assert(loadstring("return function(this, be, context, konstabel, saveloader, scene, controller, ...) #{moon_command} end"))()
	(...) ->
		bidons = be
		func(@owner, bidons, bidons.context, bidons.konstabel, bidons.saveloader, @owner.scene, @owner.scene.controller, ...) 

UtMethods.__index = (property) =>
	if @methods[property]
		return @methods[property]
	else if	@owner.owner
		return @owner.owner.methods[property]

UtMethods