return function(file, func)
	local json = require 'json'
	local raw_data = love.filesystem.read(file .. '.json')
	local data = json.decode(raw_data)
	func(data)
	raw_data = json.encode(data)
	love.filesystem.write(file .. '.json', raw_data)
end