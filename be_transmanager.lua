local class = require 'middleclass'

local BeTransmanager = class('BeTransmanager')

local csvparser = require 'csvparser'

function BeTransmanager:initialize(locale_ui, locale_tx)
	if not locale_ui then return nil end
	
	self.trans_uidata = {}
	-- Здесь подгружаем сразу нужный файл перевода из папки transdata
	local csv_table_ui = csvparser(love.filesystem.lines('transdata/' .. locale_ui .. '.csv'))
	for i, fields in ipairs(csv_table_ui) do
		if i ~= 1 then
			self.trans_uidata[fields[1]] = {}
			for i, v in ipairs(fields) do
				if i == 2 then
					self.trans_uidata[fields[1]].translation = v
				elseif i == 3 then
					self.trans_uidata[fields[1]].bdl_name = v
				end
			end
		end
	end
	
	if not locale_tx then return nil end
	
	self.trans_txdata = {}
	-- Здесь подгружаем сразу нужный файл перевода из папки transdata
	local csv_table_tx = csvparser(love.filesystem.lines('transdata/' .. locale_tx .. '-dialogs.csv'))
	for i, fields in ipairs(csv_table_tx) do
		if i ~= 1 then
			self.trans_txdata[fields[1]] = {}
			for i, v in ipairs(fields) do
				if i == 2 then
					self.trans_txdata[fields[1]].translation = v
				elseif i == 3 then
					self.trans_txdata[fields[1]].bdl_name = v
				end
			end
		end
	end
end

function BeTransmanager:get(id)
	local str
	xpcall(function()
		str = self.trans_uidata[id].translation
	end, function()
		error('Attempt to get undefined translation of `' .. id .. '`')
	end)
	return str
end

function BeTransmanager:get_tx(id)
	local str
	xpcall(function()
		str = self.trans_txdata[id].translation
	end, function()
		error('Attempt to get undefined translation of `' .. id .. '`')
	end)
	return str
end

function BeTransmanager:get_bdl(id)
	return self.trans_uidata[id].bdl_name
end

function BeTransmanager:get_from_bdl(bdl_name)
	for key, value in pairs(self.trans_uidata) do
		if value.bdl_name == bdl_name then
			return value.translation
		end
	end
	return nil
end

return BeTransmanager