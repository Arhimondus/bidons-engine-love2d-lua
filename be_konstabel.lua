local be = require 'bidons'

local class = require 'middleclass'
local lume = require 'lume'

local BeState = require 'be_state'
local BeScene = require 'be_scene'
local BeUiel = require 'be_uiel'

local BeKonstabel = class('BeKonstabel')

function BeKonstabel:initialize()
	self.states = {}
	self.loaders = {
		lua = BeState, -- state
		json = BeScene, -- scene
		ui = BeUiel, -- uiel
	}
	self.base_path = ''
end

function BeKonstabel:enure()
	be.gamestate.registerEvents()
	
	return self
end

function BeKonstabel:fromScene()
end

function BeKonstabel:fromState()
end

function BeKonstabel:fromUiel()
end

function BeKonstabel:dislocation(scenes_path)
	self.base_path = scenes_path .. '/'
	return self
end

function BeKonstabel:load(file_path_wo_ext)
	for extension, loader in pairs(self.loaders) do
		local file = file_path_wo_ext .. '.' .. extension
		local fileInfo = love.filesystem.getInfo(file)
		if loader and fileInfo then
			return loader(file_path_wo_ext)
        end
	end
end

function BeKonstabel:entrust(state_name, ...)
	local state = self:allocate(state_name)
	be.scene = state
	be.gamestate.switch(state, { type = 'entrust', params = ... })
end

function BeKonstabel:forceEntrust(state_name, ...)
	local state = self:allocate(state_name, nil, true)
	be.scene = state
	be.gamestate.switch(state, { type = 'entrust', params = ... })
end

function BeKonstabel:issue(state_name, ...)
	local state = self:allocate(state_name)
	be.scene = state
	be.gamestate.push(state, { type = 'entrust', params = ... })
end

function BeKonstabel:issueForce(state_name, ...)
	local state = self:allocate(state_name, nil, true)
	be.scene = state
	be.gamestate.push(state, { type = 'entrust', params = ... })
end

function BeKonstabel:removestate(state_name)
	self.states[state_name] = nil 
end

function BeKonstabel:retrieve(unload)
	if unload then
		local curstate = be.gamestate.current()
		self.states[curstate.loaded_name] = nil 
	end

	return be.gamestate.pop()
end

function BeKonstabel:restore(state)
	be.scene = BeScene:new(nil, nil, state)
	-- be.scene:reinit()
	be.gamestate.switch(be.scene, { type = 'entrust', params = {} })
end

function BeKonstabel:demonstrate(bui_name, ui_object, full_path)
	if not full_path then
		bui_name = self.base_path .. bui_name
	end
	
	ui_object = ui_object or be.gamestate.current()
	
	local bui = self:load(bui_name)
	
	table.insert(ui_object.objects[1].objects, bui.objects[1])
	
	return bui
end

function BeKonstabel:disarm(bui_object, ui_object)
	ui_object = ui_object or be.gamestate.current()
	
	table.remove(ui_object.objects[1].objects, lume.find(ui_object.objects[1].objects, bui_object))
end

function BeKonstabel:allocate(state_name, full_path, force)
--	if be.context then
--		be.context:closeCityPanels()
--	end
	
	full_path = full_path or false
	
	if not full_path then
		state_name = self.base_path .. state_name
	end
	
	local state = self.states[state_name]
	
	if not state or force then
		state = self:load(state_name)
		if not state then
			error('State `' .. state_name .. '` not found.')
		end
		if state.class.name == 'BeScene' or state.class.name == 'BeState' then
			self.states[state_name] = state
		else
			error('Attempt to load UI file as state/scene!')
		end
	end
	
	state.loaded_name = state_name
	
	return state
end

function BeKonstabel:draw()
end

function BeKonstabel:update(dt)
end

function BeKonstabel:retire()
	love.event.quit()
end

-- BeKonstabel:allocate()
-- BeKonstabel:retreat()
	
return BeKonstabel