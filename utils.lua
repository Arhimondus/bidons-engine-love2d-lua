local utils = {}

function utils.inside_rectangle(x, y, rx, ry, rw, rh)
	return x >= rx and x <= rx + rw and y >= ry and y <= ry + rh
end

function utils.inside_rectangle_o(x, y, rect)
	return x >= rect[1] and x <= rect[1] + rect[3] and y >= rect[2] and y <= rect[2] + rect[4]
end

-- function utils.point_inside_rect_xywh(x, y, rect) -- use inside_rectangle_o instead
-- 	return utils.inside_rectangle(x, y, rect[1], rect[1] + rect[3], rect[2] - 1, rect[2] + rect[4] - 1)
-- end

function utils.rects_intersect(a, b)
	local ax1 = a[1]
	local ax2 = a[1] + a[3]
	local ay1 = a[2]
	local ay2 = a[2] + a[4]
	
	local bx1 = b[1]
	local bx2 = b[1] + b[3]
	local by1 = b[2]
	local by2 = b[2] + b[4]
	
	return (ax1 < bx2) and (ax2 > bx1) and (ay1 < by2) and (ay2 > by1);
--	return (a[2] < b[2] + b[4]) or (a[2] + a[4] > b[2]) or (a[1] + a[3] < b[1]) or (a[1] > b[1] + b[3]);
end

function utils.point_inside_poly(x, y, poly)
	local function cut_ray(p, q)
		return ((p[2] > y and q[2] < y) or (p[2] < y and q[2] > y)) -- possible cut
			and (x - p[1] < (y - p[2]) * (q[1] - p[1]) / (q[2] - p[2])) -- x < cut.x
	end

	-- test if the ray crosses boundary from interior to exterior.
	-- this is needed due to edge cases, when the ray passes through
	-- polygon corners
	local function cross_boundary(p, q)
		return (p[2] == y and p[1] > x and q[2] < y)
			or (q[2] == y and q[1] > x and p[2] < y)
	end

	local v = poly
	local in_polygon = false
	local p, q = nil, v[#v]
	for i = 1, #v do
		p, q = q, v[i]
		if cut_ray(p, q) or cross_boundary(p, q) then
			in_polygon = not in_polygon
		end
	end

	return in_polygon
end

function utils.between(x, min, max)
	return x >= min and x <= max
end

function utils.select(initial_table, predicate)
	local destination_table = {}
	for i, v in ipairs(initial_table) do
		local data = predicate(v)
		table.insert(destination_table, data)
	end
	return destination_table
end

function utils.each(table, predicate)
	for i, v in ipairs(table) do
		predicate(v, i)
	end
end

function utils.findMinimal(table, field)
	local result, index = table[1], 1
	for i, v in ipairs(table) do
		if v[field] < result[field] then
			result = v
			index = i
		end
	end
	return result, index
end

function utils.twocolors(b, one, two)
	if b then
		return lume.color(one)
	else
		return lume.color(two)
	end
end

function utils.findFirst(t, predicate)
	for i, v in ipairs(t) do
		if predicate(v) then
			return v
		end
	end
	return nil
end

function utils.findFirstIndex(t, predicate)
	for i, v in ipairs(t) do
		if predicate(v) then
			return i
		end
	end
	return nil
end

function utils.findFirstOrDefault(t, predicate, default)
	for i, v in ipairs(t) do
		if predicate(v) then
			return v
		end
	end
	return default
end

function utils.remove(t, predicate)
	local index = utils.findFirstIndex(t, predicate)
	if index then
		return table.remove(t, index)
	end
	return nil
end

function utils.round(x)
	if x % 2 ~= 0.5 then
		return math.floor(x + 0.5)
	end
	return x - 0.5
end

function utils.imageZoom(image, container_width, container_height)
	if not image then return end
	
	local sc, ox, oy = 1, 0, 0
	
	local image_height = image:getHeight()
	local image_width = image:getWidth()
	
	-- Вписать по высоте
	if image_height > container_height then
		sc = container_height / image_height
	end
	
	if image_width > container_width then
		local new_sc = container_width / image_width
		if new_sc < sc then sc = new_sc end
	end
	
	image_height = image_height * sc
	image_width = image_width * sc
		
	--Выровнять по центру по высоте
	if image_height < container_height then
		oy = (container_height - image_height) / 2
	else
		oy = 0
	end
	
	--Выровнять по центру по ширине
	if image_width < container_width then
		ox = (container_width - image_width) / 2
	else
		ox = 0
	end
	
	return { type = 'zoom_image', image = image, sx = sc, sy = sc, ox = ox, oy = oy, cw = container_width, ch = container_height }
end

function utils.keyValueToArray(kv)
	local arr = {}
	for key, value in pairs(kv) do
		local newel = { key = key, value = value }
		table.insert(arr, newel)
	end
	return arr
end

function utils.min(a, b)
	if a < b then
		return a
	else
		return b
	end
end

function utils.max(a, b)
	if a > b then
		return a
	else
		return b
	end
end

function utils.offset(coords, base)
	if base ~= nil then
		return { coords[1] + base[1], coords[2] + base[2] }
	else
		return coords
	end
end

function utils.negative_offset(coords, base)
	if base ~= nil then
		return { coords[1] - base[1], coords[2] - base[2] }
	else
		return coords
	end
end

function utils.split(inputstr, sep)
	if sep == nil then
		sep = "%s"
	end
	local t={} ; i=1
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		t[i] = str
		i = i + 1
	end
	return t
end

function utils.split_full(str, sep, plain)
	-- from https://github.com/moteus/lua-split
	local b, res = 0, {}
	sep = sep or '%s+'

	assert(type(sep) == 'string')
	assert(type(str) == 'string')

	if #sep == 0 then
		for i = 1, #str do
			res[#res + 1] = string.sub(str, i, i)
		end
		return res
	end

	-- assert(not is_match_empty(sep, plain), 'delimiter can not match empty string')

	while b <= #str do
		local e, e2 = string.find(str, sep, b, plain)
		if e then
			res[#res + 1] = string.sub(str, b, e-1)
			b = e2 + 1
			if b > #str then res[#res + 1] = "" end
		else
			res[#res + 1] = string.sub(str, b)
			break
		end
	end
	return res
end

function utils.split_first(str, sep, plain)
  sep = sep or '%s+'

  assert(type(sep) == 'string')
  assert(type(str) == 'string')

  if #sep == 0 then
    return string.sub(str, 1, 1), string.sub(str, 2)
  end

--  assert(not is_match_empty(sep, plain), 'delimiter can not match empty string')

  local e, e2 = string.find(str, sep, nil, plain)
  if e then
    return string.sub(str, 1, e - 1), string.sub(str, e2 + 1)
  end
  return str
end

--function utils.get_files(dir, pattern)
--	local re = require("re")
--	local regex = re.compile(pattern) -- "r(e*)gex?"
--	local files = love.filesystem.getDirectoryItems(dir)
--	for i, v in ipairs(files) do
--		local check = regex:execute(v)
--		print ''
--	end
--end	

function utils.shallow_copy(t)
	local t2 = {}
	for k,v in pairs(t) do
		t2[k] = v
	end
	return t2
end

function utils.get_value(value, default)
	if value == nil then
		return default
	else
		return value
	end
end

function utils.default(value, default)
	return utils.get_value(value, default)
end

function utils.remove_random(array)
	local index = love.math.random(#array)
	table.remove(array, index)
end

function utils.transfer(array1, el1, array2, el2)
	local index1 = utils.find(array1, function() end)
	table.remove(array, index)
end

return utils