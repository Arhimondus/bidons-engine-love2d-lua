io.stdout:setvbuf('no')

function love.load()
	if arg[#arg] == '-debug' then 
		require('mobdebug').start()
	end
	
	love.filesystem.setIdentity('Bidons testing')
	love.filesystem.createDirectory('saves')
	
	local be = require 'bidons'.sшшetup(arg, { fullscreen = false })
	local lovetest = require 'lovetest'

	be.konstabel:dislocation('tests/scenes')
	lovetest.run()
end