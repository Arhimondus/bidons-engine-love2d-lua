local json = require 'json'

function find_single_file(pattern, directory)
	local files = love.filesystem.getDirectoryItems(directory)
	for _, file in ipairs(files) do
		local ok, _, name = file:find(pattern)
		if ok then
			local content = love.filesystem.read(directory .. '/' .. file)
			local item = json.decode(content)
			local id = file:match("^.+%.(.+)$")
			item._id = id
			return item
		end
	end
end

return {
	get_object = function(id)
		return find_single_file(id, 'objects')
	end,
	get_tag = function(id)
		return find_single_file(id, 'tags')
	end,
	get_type = function(id)
		return find_single_file(id, 'types')
	end
}