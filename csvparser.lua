-- By GRAND.MORG
return function(content)
	local csv_table = {}
	for line in content do
		local csv_row = {}
		for str in line:gmatch('([^;]+)') do
			table.insert(csv_row, str)
        end
		table.insert(csv_table, csv_row)
	end
	return csv_table
end