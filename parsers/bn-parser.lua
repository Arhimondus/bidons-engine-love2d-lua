local utf8 = require 'utf8'

return function()
	local function utf8_sub(str, i, j)
		i = utf8.offset(str, i)
		if j then
			local o = utf8.offset(str, j + 1)
			if o then
				j = o - 1
				return string.sub(str, i, j), j
			end
		else
			return string.sub(str, i), 100
		end
	end
	
	function utf8_sub2(s, i, j)
		local ui = utf8.offset(s, i)
		local uj = utf8.offset(s, j + 1) - 1
		return string.sub(s, ui, uj), ui - i + 1
	end

	local at; -- The index of the current character
	local ch; -- The current character
	local escapee = {
		['\''] = '\'',
		['\\'] = '\\',
		['/'] = '/',
		b = '\b',
		f = '\f',
		n = '\n',
		r = '\r',
		t = '\t'
	};
	local text;
	local classes;
	local extra;
	
	local throw_error = function (m)
		-- Call throw_throw_error when something is wrong
		error ('BnParser SyntaxError at ' .. at .. ': ' .. m)
	end

	local next = function (c)
		-- If a c parameter is provided, verify that it matches the current character.

		if (c and c ~= ch) then
			throw_error('Expected ' + c + ' instead of ' + ch + '');
		end

		-- Get the next character. When there are no more characters,
		-- return the empty string.

--		if at == 5487 then
--			print ''
--		end
		
--		ch = text:sub(at, 1);
		local offset
		ch, offset = utf8_sub2(text, at, 1);
		
--		if ch == nil then
--			print ''
--		end
		at = at + offset;
		return ch;
	end

	local get_type = function()
		if(ch == '(') then
			local type = pick_until(')');
			next(')');
			skipwhite();
			return type;
		end
		return nil;
	end

	local create_obj = function(class_type, obj)
		return classes[class_type]:new(extra, obj);
	end

	local pick_until = function(symbol)
		local a = "";
		while(text[at] ~= symbol) do
			a = a + next();
		end
		next();
		return a;
	end

	local number = function()
		-- Parse a number value.

		local value;

		local string = '';

		if (ch == '-') then
			string = '-';
			next('-');
		end
		while (ch >= '0' and ch <= '9') do
			string = string + ch;
			next();
		end
		if (ch == '.') then
			string = string .. '.';
			while (next() and ch >= '0' and ch <= '9') do
				string = string .. ch;
			end
		end
		if (ch == 'e' or ch == 'E') then
			string = string .. ch;
			next();
			if (ch == '-' or ch == '+') then
				string = string .. ch;
				next();
			end
			while (ch >= '0' and ch <= '9') do
				string = string .. ch;
				next();
			end
		end
		value = tonumber(string);
		if (not isFinite(value)) then
			throw_error('Bad number');
		else
			return value;
		end;
	end

	local hex = function()
		-- Parse a hex value.
		local value;
		local string = '';

		next('#');

		while ((ch >= '0' and ch <= '9') or (ch >= 'a' and ch <= 'f')) do
			string = string .. ch;
			next();
		end

		return string;
	end

	local string_mod = function()
		local string = '';

		skipwhite();
		next('$');

		if(ch == '{') then
			string = pick_until('}');
			next('}');
		else
			string = ch .. pick_until('\r');
		end

		return string;
	end

	local key_token = function()
		-- Parse a key value.

		local hex;
		local i;
		local value = '';
		local uffff;

		-- When parsing for string values, we must look for chars and dot

		if (ch ~= ' ') then
			value = value .. ch;
			while (next()) do
				if (ch == ' ' or ch == ')') then
					next();
					return value;
				end
				
				value = value .. ch;
			end
		end
		throw_error('Bad string');
	end

	local skipwhite = function()
		-- Skip whitespace.

		while (ch ~= nil and ch ~= '' and string.byte(ch) <= string.byte(' ')) do
			next();
		end
	end

	local newline = function()
		if(ch == '\r') then
			next('\r');
			next('\n');
		end
		if(ch == '\n') then
			next('\n');
		end
	end

	local ifnewline = function()
		return text[at + 1] == '\r' and text[at + 2] == '\n';
	end

	local word = function()
		-- true, false, nil or auto

		if ch == 't' then
			next('t');
			next('r');
			next('u');
			next('e');
			return true;
		elseif ch == 'f' then
			next('f');
			next('a');
			next('l');
			next('s');
			next('e');
			return false;
		elseif ch == 'n' then
			next('n');
			next('u');
			next('l');
			next('l');
			return nil;
		elseif ch == 'a' then
			next('a');
			next('u');
			next('t');
			next('o');
			return nil;
		end

		throw_error('Unexpected ' .. ch .. '');
	end

	local value;  -- Place holder for the value function.
	local value_ws;

	local array = function()
		-- Parse an array value.
		local arr = {};

		if (ch == '[') then
			next('[');
			skipwhite();
			if (ch == ']') then
				next(']');
				return arr;   -- empty array
			end
			while (ch) do
				local class_type = get_type();
				local val = value();
				if(class_type) then
					val = create_obj(class_type, val);
				end
				arr.push(val);
				newline();
				skipwhite();
				if (ch == ']') then
					next(']');
					return arr;
				end
				skipwhite();
			end
		end
		throw_error('Bad array');
	end

	local object = function()
		-- Parse an object value.

		local key;
		local obj = {};

		if (ch == '{') then
			next('{');
			skipwhite();
			if (ch == '}') then
				next('}');
				return obj;   -- empty object
			end
			while (ch) do
				local class_type = nil;
				key = key_token();
				skipwhite();
				-- next(':');
				if (Object.hasOwnProperty.call(obj, key)) then
					throw_error('Duplicate key ' + key + '');
				end
				
				if(ch == '(') then
					next('(');
					class_type = key_token();
				end
				local values = {};
				while(true) do
					skipwhite();
					local val = value_ws();
					if(class_type) then
						val = create_obj(class_type, val);
					end
					values.push(val);
					if(ch ~= ' ') then
						break;
					end
				end
				obj[key] = values.length > 1 and values or values[1];
				newline();
				skipwhite();
				if (ch == '}') then
					next('}');
					return obj;
				end
				-- class_type = nil;
			end
		end
		throw_error('Bad object');
	end

	value = function()
		-- Parse a JSON value. It could be an object, an array, a string, a number,
		-- or a word.

		skipwhite();
		if ch == '{' then
			return object();
		elseif ch == '[' then
			return array();
		elseif ch == '$' then
			return string_mod();
		elseif ch == '-' then
			return number();
		elseif ch == '#' then
			return hex();
		else
			if ch then
				return (string.byte(ch) >= string.byte('0') and string.byte(ch) <= string.byte('9'))
					and number()
					or word();
			else
				return word();
			end
		end
	end

	value_ws = function()
		if ch == '{' or ch == '(' then
			return object();
		elseif ch == '[' then
			return array();
		elseif ch == '$' then
			return string_mod();
		elseif ch == '-' then
			return number();
		elseif ch == '#' then
			return hex();
		else
			return (ch >= '0' and ch <= '9')
				and number()
				or word();
		end
	end

	return function(source, all_classes, extra_argument)
		local result;

		text = '{' .. source .. '}';
		classes = all_classes;
		extra = extra_argument;
		
		at = 1;
		ch = ' ';
		result = value();
		skipwhite();
		if (ch) then
			throw_error('Syntax throw_error');
		end

		return result;
	end
end