local be = require 'bidons'
local lume = require 'lume'

local json = require 'json'
local class = require 'middleclass'
local binser = require 'binser'

--- Class: BeSaveloader
--- Save/load game state
local BeSaveloader = class('BeSaveloader')

table.filter = function(t, filterIter)
  local out = {}

  for k, v in pairs(t) do
    if filterIter(v, k, t) then table.insert(out, v) end
  end

  return out
end

--[[
	Function: BeSaveloader:initialize
	Initialize new instance of save/load module

	Parameters:
		extended_classes - List of additional game classes (with full dot pathes) needed to save/load
	  
	Returns:
		BeSaveloader 
]]
function BeSaveloader:initialize(extended_classes)
	binser.registerClass(require 'be_scene', 'be_scene')
	binser.registerClass(require 'be_state', 'be_state')
	binser.registerClass(require 'be_transmanager', 'be_transmanager')
	
	local elements = require 'elements'
	for key, value in pairs(elements) do
		binser.registerClass(elements[key], key)
	end
	
	if not be.production and extended_classes then
		for _, className in ipairs(extended_classes) do
			binser.registerClass(require(className), className:contains('%.') and className:split_half_end('.')[1] or className)
		end		
	end

--	binser.registerClass(require 'ge_chapter', 'ge_chapter')
--	binser.registerClass(require 'ge_char', 'ge_char')
--	binser.registerClass(require 'ge_context', 'ge_context')
--	binser.registerClass(require 'ge_item', 'ge_item')
--	binser.registerClass(require 'ge_player', 'ge_player')
end

function BeSaveloader:quickSave(pop, auto)
	pop = pop and true or false
	local state = be.gamestate.first()
	
	local save_object = {
		stamp = state.preserve and state:preserve() or nil,
		state = state.name,
		state_full = state:pack(),
		context = be.context,
		version = love.window.getTitle():gsub("Patreon ", "")
	}
	local serstr = binser.serialize(save_object)
	local save_name = os.time()
	love.filesystem.write("saves/" .. save_name .. ".binser", serstr)
	if pop then
		be.gamestate.pop() -- Скорее всего нужно будет заменить на be.switch(state)
	end
	love.graphics.captureScreenshot("saves/" .. save_name .. "S.png")
	
	love.filesystem.write("saves/" .. save_name .. ".meta", json.encode({
		day = be.context.day,
		daypart = be.context.daypart,
		state_name = save_object.state,
		version = (require 'bidons').gameVersion,
		auto = auto
	}))
	
	be.stacklog:info('Saving game to ' .. save_name)
end

function BeSaveloader:quickLoad(save_path, callback)
	be.stacklog:info('Load from ' .. (save_path and save_path or 'last save'))
	
	local currentVersion = love.window.getTitle():gsub("Patreon ", "")
	
	if not save_path then
		local files = table.filter(love.filesystem.getDirectoryItems("saves"), function(f) return string.find(f, ".binser") end)
		if #files == 0 then
			return
		end
		save_path = files[#files]
	end

	local content = love.filesystem.read("saves/" .. save_path)
	local deserialized = binser.deserialize(content)[1]
	
	local meta_path = save_path:gsub('.binser', '.meta')
	local meta = json.decode(love.filesystem.read('saves/' .. meta_path))
	
	if meta.version ~= (require 'bidons').gameVersion then
		return false
	end
	
	be.context = deserialized.context
--	deserialized.state:restore(deserialized.stamp)

	be.konstabel:restore(deserialized.state_full)
	
	-- be.gamestate.switch(be.konstabel:allocate(deserialized.state, true), deserialized.stamp)
	
	if callback and type(callback) == 'function' then
		callback(deserialized.state_full, be.konstabel)
	end

	return true
end

function BeSaveloader:longLoad(save_file)
	local currentVersion = love.window.getTitle():gsub("Patreon ", "")
	
	local content = love.filesystem.read("saves/" .. save_file .. ".binser")
	local deserialized = binser.deserialize(content)[1]
	
	if deserialized.version ~= currentVersion then
		return false
	end
	
	be.context = deserialized.context
--	deserialized.state:restore(deserialized.stamp)
	be.gamestate.switch(be.konstabel:allocate(deserialized.state, true), deserialized.stamp)
	
	return true
end

function BeSaveloader:listOfSaves()
	local files = table.filter(love.filesystem.getDirectoryItems("saves"), function(f) return string.find(f, ".binser") end)
	if #files == 0 then
		return {}
	end
	local saves = {}
	for i, v in lume.ripairs(files) do
		local fileName = v:gsub('.binser', '')
		local meta = json.decode(love.filesystem.read('saves/'.. fileName .. '.meta'))
		table.insert(saves, {
			file = v,
			title = (#files - i + 1) .. '. ' .. require('date')(tonumber(fileName)):tolocal():fmt("%d.%m.%y %H:%M:%S") ..
			' ' .. be.transmanager:get('city_day') .. ' ' .. meta.day .. ', ' .. be.context:getDayPartName(meta.daypart) .. (meta.auto and ', auto' or '')
		}) -- "%I:%M:%S %p" - для буржуев 06:55:15 AM
	end
	return saves
end

return BeSaveloader