_goto = (label) =>
	for index, line in ipairs @lines
		line = line\trim!\gsub("%/%w+%/", "")
		if line\take(1) == '#'
			if line\skip(1)\trim! == label
				@inside_block = false
				@position = index - 1
				return

_return = (func) =>
	@script = nil
	@type = nil
	func()

nothing = () =>
	if @inside_block
		@position = @inside_block_outer - 1
		@inside_block = false
	@next()

-- shortcut for @
shortcut = (name) =>
	return 'dialog.scene.context.'

-- shortcut for &
shortcut2 = (name) =>
	return 'dialog.scene.bidons.'

sprite = (sprite) =>
	@sprite = 'assets/sprites/' .. sprite .. '.png'

sprite_off = () =>
	@sprite = nil

scene = (scene, params) =>
	@end_dialog!
	@scene.bidons.konstabel\entrust(scene, params)

virtual_bnf = (self) =>
	return nil
	
update = (dialog) =>
	for _, value in pairs @scene.raw_list
		value\run('updateBinding')

mark = (mark) =>
	@scene.bidons.context.marks[@script][mark]

mark_out = (mark) =>
	@scene.bidons.context.marks[@script][mark] - 1

set_mark = (mark, value) =>
  value = value and value or true
  @scene.bidons.context.marks[@script][mark] = value

inc_mark = (mark, value) =>
  value = value and value or 1
  @scene.bidons.context.marks[@script][mark] = @scene.bidons.context.marks[@script][mark] + value

dec_mark = (mark, value) =>
  value = value and value or 1
  @scene.bidons.context.marks[@script][mark] = @scene.bidons.context.marks[@script][mark] - value

return {
	:background
	:condition
	:close
	:clothes
	:clothes_default
	:emotion
	:emotion_default
	['goto']: _goto
	return: _return
	:set_char
	:menu_mode
	:nothing
	:shortcut
	:shortcut2
	:sprite
	:sprite_off
	:scene
	:update
	:virtual_bnf
	:mark
	:set_mark
	:inc_mark
	:dec_mark
}