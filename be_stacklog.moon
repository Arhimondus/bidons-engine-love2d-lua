be = require 'bidons'
inspect = require 'inspect'
log = require 'log'

BeStacklog = (require 'middleclass')('BeStacklog')

BeStacklog.initialize = () =>
	log.level = 'info'
	
BeStacklog.message = (object, action, data = nil) =>
	if data != nil and action != nil
		"#{tostring(object)}.#{action} = #{inspect.inspect(data)}"
	else if action != nil
		"#{tostring(object)}.#{action}"
	else
		"#{tostring(object)}"

BeStacklog.trace = (...) =>
	log.trace(@message(...))

BeStacklog.info = (...) =>
	log.info(@message(...))
	
BeStacklog.debug = (...) =>
	log.debug(@message(...))
	
BeStacklog.error = (...) =>
	log.error(@message(...))
	
BeStacklog.write_table = (object) =>
	log.debug(inspect.inspect(object))

BeStacklog