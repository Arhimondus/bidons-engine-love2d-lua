local be = require 'bidons'
local class = require 'middleclass'
local utils = require 'utils'
local json = require 'json'
local BeState = require 'be_state'
local NeBox = require 'elements.ne-box'
local bnparser = require 'parsers.bn-parser'
local elements = require 'elements'

local flux = require 'flux'
local timer = require 'timer'

local BeScene = class('BeScene', NeBox)

function BeScene:single_bscn(scene_name)
	be.stacklog:debug('BeScene:single_bscn [' .. scene_name .. ']')
	
	local base_path = be.konstabel.base_path
	local extension = '.json'
	local content = love.filesystem.read(base_path .. scene_name .. extension)
	local scene = json.decode(content)
	return scene.root_node
end

function BeScene:initialize(scene_name, extension, scene)
	local packed_scene = false
	
	if scene then
		packed_scene = true
	end

	self.raw_list = {}
	self.objects = {}
	source = {
		name = 'BeScene'
	}
	NeBox.initialize(self, self, source, base, binding_object, binding_index, owner)
	
	local full_state_element = nil
	
	if not scene then
		be.stacklog:debug('BeScene:initilize [' .. scene_name .. ']')
		
		extension = extension or '.json'
		
		local content = love.filesystem.read(scene_name .. extension)
		scene = json.decode(content)
		scene.root_node.root_node = true
		
		self.name = scene_name
	else
		be.stacklog:debug('BeScene:initilize [' .. (scene.name or 'undefined') .. '] from full state')
	end
	
	self.source = scene
	
	if scene.controller and type(scene.controller) == 'string' then
		self.controller = assert(loadstring(scene.controller))()
	end

	full_state_element = self:loadElement(scene.root_node, nil, nil, nil, self)
	table.insert(self.objects, full_state_element)
	
	if scene.controller and type(scene.controller) == 'string' then
		self.controller:on_start(self)
	end
	
	if scene.controller and type(scene.controller) ~= 'string' then
		self.controller = scene.controller
	end

	self.bidons = be
	self.context = be.context
	
	self.dragged_items = {}

	if packed_scene then
		for key, value in pairs(self.raw_list) do
			if value.unpack then value:unpack() end
		end
	end
	
	self.load = self:new_script('load')
	-- self:run 'load'
end

function BeScene:get_root()
	return self.objects[1]
end

function BeScene:pack()
	local packed_scene = {}
	packed_scene.root_node = self.objects[1]:pack()
	packed_scene.options = self.source.options
	packed_scene.name = self.name
	packed_scene.controller = self.controller
	return packed_scene
end

function BeScene:get(name)
	return self.raw_list[name]
end

function BeScene:init(ab, cd, ef)
	
end

function BeScene:enter(prev, konpam)
	BeState.enter(self, prev)
	
	self.bidons = be
	self.context = be.context
	
	if be.defaultCursor then
		love.mouse.setCursor(be.defaultCursor[1], be.defaultCursor[2], be.defaultCursor[3])
	end
	if konpam and konpam.type == 'entrust' then
		be.stacklog:info('Entrust scene `' .. (utils.default(self.name, 'undefined')) .. '`')
		self:run('load', konpam.params)
		self.objects[1]:run('load', konpam.params)
	else
		be.stacklog:info('Reenter scene `' .. (utils.default(self.name, 'undefined')) .. '`')
		self:run('enter', konpam.params)
	end
end

function BeScene:update(dt)
	flux.update(dt)
	
	status, err = pcall(function()
		timer.update(dt)
	end)
	
	NeBox.update(self, dt)
	
--	if self.raw_list['customer_bg#0'] then
--		print(self.raw_list['customer_bg#0'].absx .. ' ' .. self.raw_list['customer_bg#0'].absy .. ' / '
--			.. self.raw_list['customer_bg#0'].xcord .. ' '
--			.. self.raw_list['customer_bg#0'].ycord)
--	end
end

function BeScene:draw()
	for _, v in ipairs(self.objects) do
		v:run('draw')
	end
--	for _, v in ipairs(self.dragged) do
--		v.object:run('draw')
--	end
end

function BeScene:loadElement(element, ...)
	if not element._class:starts_with('partial:') then
		return elements[string.lower(element._class)]:new(self, element, ...)
	else
		local partialScene = element._class:split_get('partial:', 2)
		local sourceElement = self:single_bscn(partialScene)
		
		if sourceElement.name then
			sourceElement.name = sourceElement.name .. love.timer.getTime()
		end
		
		for key, value in pairs(element) do
			if key ~= '_class' then
				sourceElement[key] = value
			end
		end
		
		local newel = self:loadElement(sourceElement, ...)
		return newel
	end
end

function BeScene:mousepressed(...)
	NeBox.mousepressed(self, ...)
end

function BeScene:mousemoved(...)
	NeBox.mousemoved(self, ...)
	self:global_event('gmove', ...)
end

function BeScene:global_event(event_name, ...)
	for _, value in pairs(self.raw_list) do
		if value.behaviors then
			for _, balue in pairs(value.behaviors) do
				if balue.use_global_events and balue['_' .. event_name] then
					balue['_' .. event_name](balue, ...)
				end
			end
		end
	end
end

return BeScene